use std::collections::{HashMap, VecDeque};
use log::trace;

pub fn decode_kv_pairs(reply: impl AsRef<str>) -> Option<HashMap<String, String>> {
    let mut map = HashMap::new();
    for token in reply.as_ref().split(';') {
        if token.trim().is_empty() { continue; }

        let pair: Vec<&str> = token.split(":").collect();
        if pair.len() != 2 {
            trace!("array decoding failed: pair.len != 2");
            return None;
        }

        let key = pair[0];
        let value = pair[1];

        map.insert(key.to_string(), value.to_string());
    }

    if map.is_empty() {
        None
    } else {
        Some(map)
    }
}