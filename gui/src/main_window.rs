use gtk::prelude::*;
use gtk::{Application, ApplicationWindow, Button, Entry, Orientation, ScrolledWindow, TextBuffer, TextView};
use log::trace;
use crate::{app, ui, worker};

pub struct MainWindow {
    window: ApplicationWindow,
}

async fn handle_connect() {
    app().create_debugger().await;
}

impl MainWindow {
    pub fn create(gtk_app: &Application) -> Self {
        let vbox = gtk::Box::builder().orientation(Orientation::Vertical).build();
        let command_hbox = gtk::Box::builder().orientation(Orientation::Horizontal).build();
        let toolbar = Self::setup_toolbar(gtk_app);

        let editbox = Entry::builder()
            .hexpand(true)
            .build();

        let command_button = Button::builder()
            .label("Send")
            .margin_start(4)
            .margin_end(4)
            .margin_top(4)
            .margin_bottom(4)
            .build();

        let log = TextView::builder()
            .editable(false)
            .vexpand(true)
            .monospace(true)
            .left_margin(4)
            .right_margin(4)
            .top_margin(4)
            .bottom_margin(4)
            .build();

        let log_scroll = ScrolledWindow::builder()
            .child(&log)
            .build();

        let buffer = log.buffer();
        app().log_widget = Some(log);
        app().log_buffer = Some(buffer.clone());

        vbox.set_margin_start(4);
        vbox.set_margin_end(4);
        vbox.set_margin_top(4);
        vbox.set_margin_bottom(4);
        editbox.set_margin_top(4);

        command_hbox.append(&editbox);
        command_hbox.append(&command_button);

        vbox.append(&toolbar);
        vbox.append(&log_scroll);
        vbox.append(&command_hbox);

        let window = ApplicationWindow::builder()
            .application(gtk_app)
            .title("Snowflake")
            .child(&vbox)
            .build();

        window.set_default_size(600, 400);
        Self {
            window,
        }
    }

    fn setup_toolbar(gtk_app: &Application) -> gtk::Box {
        let hbox = ui::hbox();
        let button_connect = Button::builder()
            .label("Connect")
            .build();

        button_connect.connect_clicked(move |_| {
            worker().spawn(async {
                handle_connect().await;
            });
        });

        hbox.append(&button_connect);
        hbox.set_margin_bottom(4);
        hbox
    }

    pub fn show(&self) {
        self.window.present();
    }
}