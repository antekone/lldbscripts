use std::collections::{HashMap, VecDeque};
use std::future::Future;
use std::io::{BufRead, BufReader, BufWriter, Read, Write};
use std::net::TcpStream;
use std::pin::Pin;
use std::sync::{Arc, Mutex};
use std::sync::mpsc::{channel, Receiver};
use std::task::{Context, Poll, Waker};
use std::thread::sleep;
use std::time::Duration;
use pin_project::pin_project;

use byteorder::{LE, ReadBytesExt, WriteBytesExt};
use gtk::gio::Socket;
use hex::FromHex;
use log::{debug, error, info, trace, warn};

use crate::{app, AppError, AppResult};
use crate::wire_utils;

pub struct Connection {
    stream: BufReader<TcpStream>,
}

impl Connection {
}

#[derive(Debug, Clone)]
pub enum RemoteCommand {
    NoAckMode,
    EnableErrorStrings,
    HostInfo,
    GDBServerVersion,
    WireResponse(Arc<Mutex<WireMessage>>),
}

#[derive(Debug, Clone)]
enum DebuggerState {
    Panic(String),
    WaitForConnection, Connected,
    ExpectAck(String),
    ExpectResponse(RemoteCommand),
    SendWireMessage(Arc<Mutex<WireMessage>>),
}

macro_rules! panic_state_ret {
    ($($arg:tt)*) => {{
        let res = format!($($arg)*);
        return Ok(WireStateSequence::panic(res));
    }}
}

macro_rules! panic_state {
    ($($arg:tt)*) => {{
        let res = format!($($arg)*);
        WireStateSequence::panic(res)
    }}
}

fn send_command(w: &mut impl Write, data: impl AsRef<str>) -> AppResult<()> {
    let mut ck: u32 = 0;
    let s = data.as_ref().as_bytes();
    for ch in s {
        ck += *ch as u32;
    }

    w.write("$".as_bytes())?;
    w.write(data.as_ref().as_bytes())?;
    w.write("#".as_bytes())?;

    let ck_str = format!("{:x}", ck & 0xff);
    w.write(ck_str.as_bytes())?;
    w.flush()?;

    Ok(())
}

pub struct WireMessageMap {
    receiver: Receiver<Arc<Mutex<WireMessage>>>,
}

impl WireMessageMap {
    pub fn new(receiver: Receiver<Arc<Mutex<WireMessage>>>) -> Self {
        Self {
            receiver
        }
    }
}

#[derive(Debug)]
struct WireState {
    seq: WireStateSequence
}

impl WireState {
    pub fn new(initial_state: DebuggerState) -> Self {
        let mut seq = WireStateSequence::new();
        seq.push(initial_state);
        Self { seq }
    }

    pub fn maybe_push(&mut self, src: Option<DebuggerState>) {
        if let Some(new_state) = src {
            self.seq.push(new_state)
        }
    }

    pub fn push(&mut self, src: DebuggerState) {
        if let DebuggerState::Panic(_) = src {
            self.seq.clear();
        }

        self.seq.push(src)
    }

    pub fn push_many(&mut self, src: WireStateSequence) {
        for e in src.seq {
            if let DebuggerState::Panic(_) = e {
                self.seq.clear();
                self.seq.push(e);
                return;
            } else {
                self.seq.push(e)
            }
        }
    }

    pub fn push_many_top(&mut self, src: WireStateSequence) {
        for e in src.seq {
            if let DebuggerState::Panic(_) = e {
                self.seq.clear();
                self.seq.push(e);
                return;
            } else {
                self.seq.prepend(e)
            }
        }
    }

    pub fn current(&self) -> Option<DebuggerState> {
        self.seq.peek()
    }

    pub fn next(&mut self) -> Option<DebuggerState> {
        self.seq.pop_front()
    }

    pub fn panic(&mut self, reason: impl AsRef<str>) {
        self.seq.clear();
        self.seq.push(DebuggerState::Panic(reason.as_ref().to_string()));
    }

    pub fn unimplemented(&mut self) {
        self.seq.clear();
        self.seq.push(DebuggerState::Panic("Unimplemented state".into()));
    }
}

#[derive(Debug)]
struct WireStateSequence {
    seq: VecDeque<DebuggerState>,
}

impl WireStateSequence {
    pub fn new() -> Self {
        Self { seq: VecDeque::new() }
    }

    pub fn panic(reason: impl AsRef<str>) -> Self {
        let mut s = Self::new();
        s.push(DebuggerState::Panic(reason.as_ref().to_string()));
        s
    }

    pub fn prepend(&mut self, state: DebuggerState) {
        self.seq.insert(0, state);
    }

    pub fn push(&mut self, state: DebuggerState) {
        self.seq.push_back(state);
    }

    pub fn pop(&mut self) -> Option<DebuggerState> {
        self.seq.pop_back()
    }

    pub fn pop_front(&mut self) -> Option<DebuggerState> {
        self.seq.pop_front()
    }

    pub fn peek(&self) -> Option<DebuggerState> {
        if let Some(s) = self.seq.front() {
            Some(s.clone())
        } else {
            None
        }
    }

    pub fn clear(&mut self) {
        self.seq.clear();
    }

    pub fn len(&self) -> usize { self.seq.len() }
}

impl From<Vec<DebuggerState>> for WireStateSequence {
    fn from(src: Vec<DebuggerState>) -> Self {
        let mut seq = WireStateSequence::new();
        for e in src {
            seq.push(e);
        }
        seq
    }
}

struct WireIO {
    reader: Option<BufReader<TcpStream>>,
    writer: Option<BufWriter<TcpStream>>,
    ack_needed: bool,
    error_strings: bool,
}

impl WireIO {
    fn new() -> Self {
        Self {
            reader: None, writer: None,
            ack_needed: true,
            error_strings: false,
        }
    }

    fn set_reader(&mut self, rdr: BufReader<TcpStream>) {
        self.reader = Some(rdr);
    }

    fn set_writer(&mut self, wr: BufWriter<TcpStream>) {
        self.writer = Some(wr);
    }

    fn write_str(&mut self, buf: impl AsRef<str>) -> std::io::Result<usize> {
        let bytes = self.write(buf.as_ref().as_bytes())?;
        Ok(bytes)
    }

    fn write_rich_str(&mut self, buf: impl AsRef<str>) -> std::io::Result<usize> {
        self.write_u8(b'$')?;
        let bytes = self.write(buf.as_ref().as_bytes())?;
        self.write_u8(b'#')?;
        let csum = Self::calc_checksum(buf.as_ref().as_bytes());
        self.write(format!("{:02X}", csum).as_bytes());
        trace!("--> ${}#{:02X}", buf.as_ref(), csum);
        Ok(bytes)
    }

    fn send_ack(&mut self, buf: impl AsRef<str>) -> std::io::Result<DebuggerState> {
        self.write_rich_str(buf.as_ref())?;
        Ok(DebuggerState::ExpectAck(buf.as_ref().to_string()))
    }

    fn send_raw(&mut self, buf: impl AsRef<str>) -> std::io::Result<()> {
        let _ = self.write(buf.as_ref().as_bytes())?;
        trace!("--> {}", buf.as_ref());
        Ok(())
    }

    fn expect_response(&mut self, rcmd: RemoteCommand) -> DebuggerState {
        DebuggerState::ExpectResponse(rcmd)
    }

    fn read_until(
        &mut self,
        end_marker: u8,
        max_size: usize,
        out: &mut Vec<u8>
    ) -> AppResult<()> {
        let mut bytes_read = 0;
        loop {
            let c = self.read_u8()?;
            if c == end_marker {
                break;
            }

            out.push(c);

            bytes_read += 1;
            if bytes_read >= max_size {
                let s = format!("Data packet is too large: {} bytes", bytes_read);
                return Err(AppError::Protocol(s));
            }
        }

        Ok(())
    }

    fn calc_checksum(msg: &[u8]) -> u8 {
        msg.iter().fold(0, |mut acc, b| {
            acc = acc.wrapping_add(*b); acc
        })
    }

    fn verify_checksum(msg: &[u8], checksum_str: &[u8; 2]) -> bool {
        let valid_csum = Self::calc_checksum(msg);
        let csum = <[u8; 1]>::from_hex(checksum_str).unwrap_or([0xFFu8]);
        csum[0] == valid_csum
    }
}

impl Read for WireIO {
    fn read(&mut self, buf: &mut [u8]) -> std::io::Result<usize> {
        if let Some(rdr) = self.reader.as_mut() {
            rdr.read(buf)
        } else {
            panic!("Reader is unavailable");
        }
    }
}

impl Write for WireIO {
    fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
        if let Some(wr) = self.writer.as_mut() {
            wr.write(buf)
        } else {
            panic!("Writer is unavailable");
        }
    }

    fn flush(&mut self) -> std::io::Result<()> {
        if let Some(wr) = self.writer.as_mut() {
            wr.flush()
        } else {
            panic!("Writer is unavailable");
        }
    }
}

fn init_session(
    io: &mut WireIO
) -> AppResult<WireStateSequence> {
    io.send_raw("+")?;
    let ack1 = io.send_ack("QStartNoAckMode")?;
    io.send_raw("+")?;

    let ret = vec![
        ack1,
        io.expect_response(RemoteCommand::NoAckMode),
        // io.send_ack("QEnableErrorStrings")?,
        // io.expect_response(RemoteCommand::EnableErrorStrings),
        io.send_ack("qGDBServerVersion")?,
        io.expect_response(RemoteCommand::GDBServerVersion),
        io.send_ack("qHostInfo")?,
        io.expect_response(RemoteCommand::HostInfo),
    ].into();

    io.flush()?;
    Ok(ret)
}

fn handle_ack(
    io: &mut WireIO,
    last_cmd: &String
) -> AppResult<WireStateSequence> {
    if io.ack_needed == true {
        trace!("Waiting for ACK...");
        let ch = io.read_u8()?;
        return if ch == b'+' {
            // ok
            Ok(WireStateSequence::new())
        } else if ch == b'-' {
            // retransmission request
            Ok(vec![io.send_ack(last_cmd)?].into())
        } else {
            panic_state_ret!("Communication loop state error: unknown ACK \
            response: 0x{:02X}", ch);
        }
    } else {
        // ACK is turned off, so don't wait for it, because it'll never
        // come.
        trace!("ACK disabled, continuing");
        Ok(WireStateSequence::new())
    }
}

fn handle_enable_error_strings(
    io: &mut WireIO,
    state: &mut WireState,
    reply: &[u8]
) -> AppResult<()> {
    if reply == [b'O', b'K'] {
        trace!("Entered ErrorStrings mode");
        io.error_strings = true;
    } else {
        warn!("Couldn't enable ErrorStrings mode, error messages will not \
            be as rich as they could");
    }

    Ok(())
}

fn handle_host_info(
    io: &mut WireIO,
    state: &mut WireState,
    reply: &[u8]
) -> AppResult<()> {
    let reply = String::from_utf8_lossy(reply).to_string();
    let items = wire_utils::decode_kv_pairs(reply);
    trace!("host info: {:?}", items);
    Ok(())
}

fn handle_gdb_server_version(
    io: &mut WireIO,
    state: &mut WireState,
    reply: &[u8]
) -> AppResult<()> {
    let reply = String::from_utf8_lossy(reply).to_string();
    trace!("gdb server version: {:?}", reply);
    Ok(())
}

fn handle_reply_no_ack_mode(
    io: &mut WireIO,
    state: &mut WireState,
    reply: &[u8]
) -> AppResult<()> {
    if reply == [b'O', b'K'] {
        trace!("Entered NoAck mode");
        io.ack_needed = false;
    } else {
        let s = format!("unsupported protocol: the server doesn't \
            understand how to enter NoAck mode, which is required");

        state.push(DebuggerState::Panic(s));
    }

    Ok(())
}

fn dispatch_response(
    io: &mut WireIO,
    state: &mut WireState,
    remote_cmd: RemoteCommand,
    reply: &[u8]
) -> AppResult<()> {
    match remote_cmd {
        RemoteCommand::NoAckMode =>
            handle_reply_no_ack_mode(io, state, reply)?,
        RemoteCommand::EnableErrorStrings =>
            handle_enable_error_strings(io, state, reply)?,
        RemoteCommand::HostInfo =>
            handle_host_info(io, state, reply)?,
        RemoteCommand::GDBServerVersion =>
            handle_gdb_server_version(io, state, reply)?,
        RemoteCommand::WireResponse(msg) => {
            let mut msg = msg.lock().unwrap();
            msg.response = Some(String::from_utf8_lossy(reply).into());
            if let Some(waker) = msg.waker.take() {
                waker.wake();
            }
        }
    }

    Ok(())
}

fn handle_response(
    io: &mut WireIO,
    state: &mut WireState,
    remote_cmd: RemoteCommand,
) -> AppResult<()> {
    let c = io.read_u8()?;
    if c == b'$' {
        let mut buf = Vec::new();
        let mut checksum_str = [0u8; 2];

        io.read_until(b'#', 4_000_000, &mut buf)?;
        io.read_exact(&mut checksum_str);

        trace!("<-- '${}#{}{}'", String::from_utf8_lossy(&buf),
            checksum_str[0] as char, checksum_str[1] as char);

        let msg_valid = WireIO::verify_checksum(&buf, &checksum_str);
        if !msg_valid {
            // let got_csum = <[u8; 1]>::from_hex(checksum_str).unwrap_or([0xFFu8]);
            // let valid_csum = WireIO::calc_checksum(&buf);
            // state.push_many(
            //     panic_state!("last incoming packet had an invalid checksum: \
            //     {:?}, expected {:?}", got_csum[0], valid_csum));
            warn!("last packet had invalid checksum");
        }

        dispatch_response(io, state, remote_cmd, &buf)?;
        io.flush();
    } else {
        state.push_many(panic_state!(
            "Communication loop state error: bad response marker \
            (missing '$', got 0x{:02x})", c));
    }

    return Ok(())
}

fn connect_to_host(
    host: impl AsRef<str>,
    io: &mut WireIO
) -> AppResult<DebuggerState> {
    let host = host.as_ref();

    debug!("Connecting to {}...", host);
    return match TcpStream::connect(host) {
        Ok(s) => {
            info!("Connected!");
            io.set_reader(BufReader::new(s.try_clone()?));
            io.set_writer(BufWriter::new(s));
            Ok(DebuggerState::Connected)
        }
        Err(msg) => {
            error!("Connection error: {:?}", msg);
            sleep(Duration::from_secs(5));
            Ok(DebuggerState::WaitForConnection)
        }
    }
}

pub fn enter_communication_loop(msg_map: Arc<WireMessageMap>) -> AppResult<()> {
    let host = "192.168.0.5:9999";

    let mut state = WireState::new(DebuggerState::WaitForConnection);
    let mut io = WireIO::new();

    loop {
        trace!("state stack: {:?}", state);
        let cur_state = state.next();
        if let None = cur_state {
            match msg_map.receiver.recv() {
                Ok(msg) => {
                    state.push(DebuggerState::SendWireMessage(msg.clone()));
                    state.push(DebuggerState::ExpectResponse(RemoteCommand::WireResponse(msg.clone())));
                }
                Err(err) => {
                    panic!("recv panic: {:?}", err);
                }
            }
        }

        let cur_state = cur_state.unwrap();
        trace!("dispatching: {:?}", cur_state);

        match cur_state {
            DebuggerState::WaitForConnection => {
                let new_state = connect_to_host(host, &mut io)?;
                state.push(new_state);
            }
            DebuggerState::Connected => {
                state.push_many(init_session(&mut io)?);
            }
            DebuggerState::ExpectAck(last_cmd) => {
                state.push_many_top(handle_ack(&mut io, &last_cmd)?);
            }
            DebuggerState::ExpectResponse(remote_cmd) => {
                handle_response(&mut io, &mut state, remote_cmd)?;
            }
            DebuggerState::Panic(reason) => {
                error!("Panic in remote handler: {}", reason);
                loop {
                    sleep(Duration::from_secs(100000));
                }
            }
            DebuggerState::SendWireMessage(wire_msg) => {
                let _ = io.write_rich_str(wire_msg.lock().unwrap().data.as_str())?;
            }
        }
    }
}

#[derive(Debug)]
pub struct WireMessage {
    pub data: String,
    pub response: Option<String>,
    pub waker: Option<Waker>,
}

#[pin_project]
pub struct WireMessageFuture {
    #[pin]
    pub msg: Arc<Mutex<WireMessage>>
}

impl Future for WireMessageFuture {
    type Output = String;

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let mut msg = self.msg.lock().unwrap();
        msg.waker = Some(cx.waker().clone());

        if msg.response.is_none() {
            return Poll::Pending;
        }

        let result = msg.response.take().unwrap();
        Poll::Ready(result)
    }
}

pub fn create_debugger(msg_map: Arc<WireMessageMap>) {
    enter_communication_loop(msg_map);
}