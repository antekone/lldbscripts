#![allow(dead_code, unused)]

use std::error::Error;
use std::future::Future;
use std::mem::MaybeUninit;
use std::pin::Pin;
use std::sync::{Arc, Mutex};
use std::sync::mpsc::{channel, Receiver, sync_channel};
use std::task::{Context, Poll, Waker};
use std::thread;
use std::thread::{sleep, spawn};
use std::time::Duration;

use futures::future::BoxFuture;
use futures::task::SpawnExt;
use gtk::{Align, Application, ApplicationWindow, Button, ButtonsType, glib, Inhibit, MessageDialog, ScrollablePolicy, Window};
use gtk::glib::{clone, MainContext};
use gtk::prelude::*;
use log::{error, info, trace};
use pin_project::pin_project;

use async_rt::*;

use crate::debugger::Debugger;
use crate::loggers::ChannelLogger;
use crate::main_window::MainWindow;
use crate::wire::{WireMessage, WireMessageMap};

mod ui;
mod async_rt;
mod wire;
mod main_window;
mod debugger;
mod loggers;
mod wire_utils;

static mut APP: MaybeUninit<AppContext> = MaybeUninit::uninit();

#[derive(thiserror::Error, Debug)]
pub enum AppError {
    #[error("General")]
    General,

    #[error("Protocol error: {0}")]
    Protocol(String),

    #[error("I/O: {0}")]
    IO(#[from] std::io::Error),
}

pub type AppResult<OK> = Result<OK, AppError>;

pub struct AppContext {
    pub dbg_: Option<Debugger>,
    pub log_buffer: Option<gtk::TextBuffer>,
    pub log_widget: Option<gtk::TextView>,
    pub log_mark: Option<gtk::TextMark>,
}

pub struct WaitForDebugger {
    pub ready: bool,
    pub waker: Option<Waker>
}

impl WaitForDebugger {
    pub fn new() -> Self {
        Self { ready: false, waker: None }
    }
}

#[derive(Clone)]
#[pin_project]
pub struct WaitForDebuggerFuture {
    #[pin]
    pub waiter: Arc<Mutex<WaitForDebugger>>
}

impl Future for WaitForDebuggerFuture {
    type Output = ();

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let mut waiter = self.waiter.lock().unwrap();
        waiter.waker = Some(cx.waker().clone());
        return if waiter.ready == true {
            Poll::Ready(())
        } else {
            Poll::Pending
        }
    }
}

impl AppContext {
    pub fn set_dbg(&mut self, dbg: Debugger) {
        self.dbg_ = Some(dbg)
    }

    pub fn dbg(&mut self) -> &mut Debugger {
        self.dbg_.as_mut().unwrap()
    }

    pub fn create_debugger(&mut self) -> WaitForDebuggerFuture {
        let mut waiter = Arc::new(Mutex::new(WaitForDebugger::new()));
        let mut waiter2 = waiter.clone();

        spawn(move || {
            let (sender, receiver) = channel::<Arc<Mutex<WireMessage>>>();
            let mut msg_map = Arc::new(WireMessageMap::new(receiver));
            app().dbg().sender = Some(sender);
            { waiter2.lock().unwrap().ready = true; }
            wire::create_debugger(msg_map);
        });

        WaitForDebuggerFuture { waiter }
    }
}

fn app_init() {
    unsafe {
        APP.write(AppContext {
            dbg_: None,
            log_buffer: None,
            log_widget: None,
            log_mark: None
        });
    }
}

pub fn app() -> &'static mut AppContext {
    unsafe { APP.assume_init_mut() }
}

fn main() {
    init();
    app_init();

    let app = Application::builder()
        .application_id("org.anadoxin.snowflake")
        .build();

    app.connect_activate(move |app| {
        build_ui(&app);
        info!("Starting snowflake v{}", env!("CARGO_PKG_VERSION"));
    });

    app.run();
}

fn setup_logging(receiver: Receiver<String>) {
    spawn(move || {
        let mut first_call = true;

        while let Ok(msg) = receiver.recv() {
            glib::idle_add_once(move || {
                if let Some(log_buffer) = app().log_buffer.as_mut() {
                    let log_widget = app().log_widget.as_ref().unwrap();

                    if first_call {
                        app().log_mark = Some(log_buffer.create_mark(
                            Some("last-line"), &log_buffer.end_iter(), false
                        ));
                        first_call = false;
                    }

                    let mark = app().log_mark.as_ref().unwrap();
                    let str = format!("{}\n", msg);
                    log_buffer.insert(&mut log_buffer.end_iter(), str.as_ref());
                    log_buffer.move_mark(mark, &log_buffer.end_iter());
                    log_widget.scroll_to_mark(mark, 0.0, false, 0.0, 0.0);
                }
            });
        }
    });
}

fn build_ui(gtk_app: &Application) {
    let wnd = MainWindow::create(gtk_app);
    wnd.show();

    let (sender, receiver) = channel::<String>();
    setup_logging(receiver);
    loggers::setup_channel_logger(sender);
}