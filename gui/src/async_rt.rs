use tokio::{join, runtime};
use tokio::runtime::{Builder, Runtime};
use std::future::Future;
use std::mem::MaybeUninit;
use std::pin::Pin;
use std::rc::Rc;
use std::sync::{Arc, Mutex};
use std::sync::mpsc::sync_channel;
use std::task::{Context, Poll, Waker};
use std::thread;
use std::thread::{sleep};
use std::time::Duration;
use gtk::glib::MainContext;
use crate::app;

static mut RT: MaybeUninit<Runtime> = MaybeUninit::uninit();

pub fn worker() -> &'static Runtime {
    unsafe { RT.assume_init_ref() }
}

pub fn gtk() -> MainContext {
    MainContext::default()
}

pub fn init() {
    unsafe {
        RT.write(Builder::new_multi_thread()
            .worker_threads(4)
            .enable_time()
            .enable_io()
            .thread_name("snowflake")
            .build()
            .unwrap());
    }
}
