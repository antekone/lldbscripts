use gtk::Orientation;

pub fn hbox() -> gtk::Box {
    gtk::Box::builder().orientation(Orientation::Horizontal).build()
}

pub fn vbox() -> gtk::Box {
    gtk::Box::builder().orientation(Orientation::Vertical).build()
}
