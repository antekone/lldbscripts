use std::mem::MaybeUninit;
use std::sync::{Arc, Mutex, RwLock};
use std::sync::mpsc::{Sender, SyncSender};
use std::time::{Instant, SystemTime};
use chrono::Timelike;
use gtk::TextBuffer;
use log::{Level, Metadata, Record};

static mut LOGGER: MaybeUninit<ChannelLogger> = MaybeUninit::uninit();

pub struct ChannelLogger {
    sender: Mutex<Sender<String>>,
}

impl ChannelLogger {
    pub fn new(sender: Sender<String>) -> Self {
        Self {
            sender: Mutex::new(sender)
        }
    }
}

impl log::Log for ChannelLogger {
    fn enabled(&self, metadata: &Metadata) -> bool {
        metadata.level() <= Level::Trace
    }

    fn log(&self, record: &Record) {
        if record.target() != "gui" && record.target().starts_with("gui::") == false {
            return;
        }

        let msg = match record.level() {
            Level::Info => format!("{}", record.args()),
            Level::Trace => {
                format!("[TRACE {}] {}",
                        record.target(),
                        record.args()
                )
            },
            Level::Warn => {
                format!("[WARNING] {}", record.args())
            },
            Level::Error => {
                format!("[ERROR] {}", record.args())
            }
            Level::Debug => {
                format!("[{}] {}",
                        record.target(),
                        record.args()
                )
            }
        };

        let now = chrono::Utc::now();
        let date_str = format!("{:02}:{:02}:{:02}",
            now.hour(),
            now.minute(),
            now.second()
        );

        let rich_log = format!("[{}] {}", date_str, msg);
        println!("{}", rich_log);

        self.sender.lock().unwrap().send(rich_log);
    }

    fn flush(&self) {}
}

pub fn setup_channel_logger(sender: Sender<String>) {
    unsafe {
        LOGGER.write(ChannelLogger::new(sender));
        log::set_logger(LOGGER.assume_init_ref());
    }

    log::set_max_level(log::LevelFilter::Trace);
}