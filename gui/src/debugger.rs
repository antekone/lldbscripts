use std::net::TcpStream;
use std::sync::{Arc, Mutex};
use std::sync::mpsc::Sender;
use crate::{app, AppResult};
use crate::wire::{Connection, WireMessage, WireMessageFuture};

pub struct Debugger {
    pub sender: Option<Sender<Arc<Mutex<WireMessage>>>>,
}

impl Debugger {
    pub fn new() -> AppResult<Self> {
        Ok(Self {
            sender: None
        })
    }

    pub fn send_something(&mut self) -> WireMessageFuture {
        let msg = Arc::new(Mutex::new(WireMessage {
            data: "qHostInfo".into(),
            response: None,
            waker: None
        }));

        self.sender.as_mut().unwrap().send(msg.clone()).unwrap();
        WireMessageFuture { msg }
    }
}