#include <gtest/gtest.h>

#include "utils/string_utils.h"

TEST(Subpatterns, Empty) {
	const auto v = string_utils::findSubpatterns("", "");
	ASSERT_TRUE(v.empty());
}

TEST(Subpatterns, OneEmpty1) {
	const auto v = string_utils::findSubpatterns("abc", "");
	ASSERT_TRUE(v.empty());
}

TEST(Subpatterns, OneEmpty2) {
	const auto v = string_utils::findSubpatterns("", "abc");
	ASSERT_TRUE(v.empty());
}

TEST(Subpatterns, Equal) {
	const auto v = string_utils::findSubpatterns("abc", "abc");
	ASSERT_EQ(1, v.size());
	ASSERT_EQ("abc", v[0]);
}

TEST(Subpatterns, Pattern1) {
	const auto v = string_utils::findSubpatterns("abcde", "abc");
	ASSERT_EQ(1, v.size());
	ASSERT_EQ("abc", v[0]);
}

TEST(Subpatterns, Pattern1R) {
	const auto v = string_utils::findSubpatterns("abc", "abcde");
	ASSERT_EQ(1, v.size());
	ASSERT_EQ("abc", v[0]);
}

TEST(Subpatterns, Pattern2) {
	const auto v = string_utils::findSubpatterns("xaabc", "abc");
	ASSERT_EQ(1, v.size());
	ASSERT_EQ("abc", v[0]);
}

TEST(Subpatterns, Pattern2R) {
	const auto v = string_utils::findSubpatterns("abc", "xaabc");
	ASSERT_EQ(1, v.size());
	ASSERT_EQ("abc", v[0]);
}

TEST(Subpatterns, Pattern3) {
	const auto v = string_utils::findSubpatterns("abc", "xaabc");
	ASSERT_EQ(1, v.size());
	ASSERT_EQ("abc", v[0]);
}

TEST(Subpatterns, Pattern3R) {
	const auto v = string_utils::findSubpatterns("xaabc", "abc");
	ASSERT_EQ(1, v.size());
	ASSERT_EQ("abc", v[0]);
}

TEST(Subpatterns, Pattern4) {
	const auto v = string_utils::findSubpatterns("abc", "abcabc");
	ASSERT_EQ(1, v.size());
	ASSERT_EQ("abc", v[0]);
}

TEST(Subpatterns, Pattern4R) {
	const auto v = string_utils::findSubpatterns("abcabc", "abc");
	ASSERT_EQ(1, v.size());
	ASSERT_EQ("abc", v[0]);
}

TEST(Subpatterns, Pattern5) {
	const auto v = string_utils::findSubpatterns("0abcxdefy", "abcdef");
	ASSERT_EQ(2, v.size());
	ASSERT_EQ("def", v[0]);
	ASSERT_EQ("abc", v[1]);
}

TEST(Subpatterns, Pattern5R) {
	const auto v = string_utils::findSubpatterns("abcdef", "0abcxdefy");
	ASSERT_EQ(2, v.size());
	ASSERT_EQ("def", v[0]);
	ASSERT_EQ("abc", v[1]);
}

TEST(Subpatterns, Bit1) {
	const auto v = string_utils::findSubpatterns("1011101", "1011101000");
	ASSERT_EQ(1, v.size());
	ASSERT_EQ("1011101", v[0]);
}

TEST(Subpatterns, Bit2) {
	const auto v = string_utils::findSubpatterns("0001011101", "1011101000");
	ASSERT_EQ(3, v.size());
	ASSERT_EQ("1011101", v[0]);
	ASSERT_EQ("010", v[1]);
	ASSERT_EQ("000", v[2]);
}

TEST(Subpatterns, Bit3) {
	const auto v = string_utils::findSubpatterns(
		"0000000000000000000000000000000000000000000000000000000001111011",
		"0000000000000000000000000000000000000000000000000000000101000001"
	);
	ASSERT_EQ(2, v.size());
	ASSERT_EQ("00000000000000000000000000000000000000000000000000000001", v[0]);
	ASSERT_EQ("101", v[1]);
}