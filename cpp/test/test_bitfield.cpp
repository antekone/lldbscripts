#include <gtest/gtest.h>
#include <string>
#include <gsl/gsl>
#include <bitset>
#include "utils/string_utils.h"

TEST(Bitfield, InsertBitsetSpaces) {
	const auto bf = std::bitset<64>(0xcafebabedeadbeef);
	const auto s = string_utils::insertBitsetSpaces(bf.to_string());
	ASSERT_EQ("11001010 11111110 10111010 10111110 11011110 10101101 10111110 11101111", s);
}

TEST(Bitfield, InsertBitsetSpaces2) {
	const auto bf = std::bitset<64>(0x0afebabedeadbeef);
	const auto s = string_utils::insertBitsetSpaces(bf.to_string());
	ASSERT_EQ("00001010 11111110 10111010 10111110 11011110 10101101 10111110 11101111", s);
}
