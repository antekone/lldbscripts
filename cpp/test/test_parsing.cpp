#include <gtest/gtest.h>
#include "utils/string_utils.h"

using namespace string_utils;

TEST(ParseAddressAndSize, Valid) {
    ASSERT_TRUE(string_utils::parseAddressAndSize("1234:5678"));
    ASSERT_TRUE(string_utils::parseAddressAndSize("1:5"));
    ASSERT_TRUE(string_utils::parseAddressAndSize("0x1:5"));
    ASSERT_TRUE(string_utils::parseAddressAndSize("1:0xF"));
    ASSERT_TRUE(string_utils::parseAddressAndSize("0xA:0xF"));
    ASSERT_TRUE(string_utils::parseAddressAndSize("0d5:0d8"));
    ASSERT_TRUE(string_utils::parseAddressAndSize("0d5:0d8:::"));
}

TEST(ParseAddressAndSize, Invalid) {
    ASSERT_FALSE(string_utils::parseAddressAndSize(""));
    ASSERT_FALSE(string_utils::parseAddressAndSize(":"));
    ASSERT_FALSE(string_utils::parseAddressAndSize("1:"));
    ASSERT_FALSE(string_utils::parseAddressAndSize(":1"));
    ASSERT_FALSE(string_utils::parseAddressAndSize("a:f"));
}

TEST(ParseAddressAndSize, ValidDetailed) {
	{
		auto a = string_utils::parseAddressAndSize("1234:5678");
		ASSERT_EQ(a->address, 1234);
		ASSERT_EQ(a->size, 5678);
	}

	{
		auto a = string_utils::parseAddressAndSize("1:5");
		ASSERT_EQ(a->address, 1);
		ASSERT_EQ(a->size, 5);
	}

	{
		auto a = string_utils::parseAddressAndSize("0x10:0x50");
		ASSERT_EQ(a->address, 0x10);
		ASSERT_EQ(a->size, 0x50);
	}

	{
		auto a = string_utils::parseAddressAndSize("0x10:50");
		ASSERT_EQ(a->address, 0x10);
		ASSERT_EQ(a->size, 50);
	}

	{
		auto a = string_utils::parseAddressAndSize("0x10:50:/something");
		ASSERT_EQ(a->address, 0x10);
		ASSERT_EQ(a->size, 50);
	}
}