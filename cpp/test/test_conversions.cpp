#include <gtest/gtest.h>
#include "utils/string_utils.h"

using namespace string_utils;

TEST(NumberConversion, Zero) {
	ASSERT_EQ(0, toUInt64("0"));
	ASSERT_EQ(0, toUInt64("0d0"));
	ASSERT_EQ(0, toUInt64("0x0"));
	ASSERT_EQ(0, toUInt64("0b0"));
}

TEST(NumberConversion, One) {
	ASSERT_EQ(1, toUInt64("1"));
	ASSERT_EQ(1, toUInt64("0d1"));
	ASSERT_EQ(1, toUInt64("0x1"));
	ASSERT_EQ(1, toUInt64("0b1"));
}

TEST(NumberConversion, Ten) {
	ASSERT_EQ(10, toUInt64("10"));
	ASSERT_EQ(10, toUInt64("0d10"));
	ASSERT_EQ(16, toUInt64("0x10"));
	ASSERT_EQ(2, toUInt64("0b10"));
}

TEST(NumberConversion, Bigger) {
	ASSERT_EQ(666, toUInt64("666"));
	ASSERT_EQ(666, toUInt64("0d666"));
	ASSERT_EQ(666, toUInt64("0x29a"));
	ASSERT_EQ(666, toUInt64("0b1010011010"));
}

TEST(NumberConversion, Max) {
	const auto x = 0xffffffffffffffff;
	ASSERT_EQ(x, toUInt64("18446744073709551615"));
	ASSERT_EQ(x, toUInt64("0d18446744073709551615"));
	ASSERT_EQ(x, toUInt64("0xffffffffffffffFF"));
	ASSERT_EQ(x, toUInt64("0b1111111111111111111111111111111111111111111111111111111111111111"));
}

TEST(NumberConversion, Invalid) {
	ASSERT_FALSE(toUInt64("1x446744073709551615"));
	ASSERT_FALSE(toUInt64("0d1844674407370955161x"));
	ASSERT_FALSE(toUInt64("0xffffffffffffffFG"));
	ASSERT_FALSE(toUInt64("0b111111111111111111111112111111111111"));
	ASSERT_FALSE(toUInt64("0y1231"));
	ASSERT_FALSE(toUInt64(""));
	ASSERT_FALSE(toUInt64(" "));
}
