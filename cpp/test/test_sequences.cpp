#include <gtest/gtest.h>
#include "../src/utils/string_utils.h"

TEST(InterpolateSequence, Simple) {
	const auto s = string_utils::interpolateSequence("testXXXX", 'X', 1234);
	ASSERT_EQ("test1234", s);
}

TEST(InterpolateSequence, SmallValue) {
	const auto s = string_utils::interpolateSequence("testXXXX", 'X', 123);
	ASSERT_EQ("test0123", s);
}

TEST(InterpolateSequence, OverflowValue) {
	const auto s = string_utils::interpolateSequence("testXXXX", 'X', 12345);
	ASSERT_EQ("test12345", s);
}

TEST(InterpolateSequence, Zeroes) {
	const auto s = string_utils::interpolateSequence("testXXXX", 'X', 0);
	ASSERT_EQ("test0000", s);
}

TEST(InterpolateSequence, One) {
	const auto s = string_utils::interpolateSequence("testX", 'X', 1);
	ASSERT_EQ("test1", s);
}

TEST(InterpolateSequence, ThreeDigits) {
	const auto s = string_utils::interpolateSequence("testX", 'X', 199);
	ASSERT_EQ("test199", s);
}

TEST(InterpolateSequence, NoPlaces) {
	const auto s = string_utils::interpolateSequence("test", 'X', 199);
	ASSERT_EQ("test", s);
}