#include <algorithm>
#include <string>
#include <cassert>
#include <fmt/core.h>

#include "string_utils.h"

std::string string_utils::interpolateSequence(
	const std::string& in, char seqCode, uint64_t seqValue
) {
	auto out = in;
	auto places = std::count(in.begin(), in.end(), seqCode);
	if (places == 0) {
		return in;
	}

	auto seqValueString = fmt::format(
		fmt::runtime(fmt::format("{{:0{}}}", places)), seqValue);

	std::reverse(seqValueString.begin(), seqValueString.end());

	auto iter = out.rbegin();
	auto seqIter = seqValueString.begin();

	auto lastValidIter = iter;
	while (iter != out.rend()) {
		if (*iter == seqCode) {
			*iter = *seqIter;
			seqIter++;
			lastValidIter = iter;
		}

		iter++;
	}

	std::for_each(seqIter, seqValueString.end(), [lastValidIter, &out] (auto c) {
		out.insert(lastValidIter.base() - 1, c);
	});

	return out;
}

template <typename Iter>
static int measureEquality(Iter s1, Iter s2, Iter s1end, Iter s2end) {
	int s = 0;
	while (s1 != s1end && s2 != s2end && *s1++ == *s2++)
		s++;
	return s;
}

bool isSubpatternOf(const std::string& curPat, const std::string& prevPat) {
	return prevPat.find(curPat) != std::string::npos;
}

std::vector<std::string> string_utils::findSubpatterns(
	const std::string& in1,
	const std::string& in2)
{
	std::vector<std::string> pats;
	std::string curPat;

	for (auto i1 = in1.begin(); i1 != in1.end(); i1++) {
		for (auto i2 = in2.begin(); i2 != in2.end(); i2++) {
			if (*i1 == *i2) {
				auto n = measureEquality(i1, i2, in1.end(), in2.end());
				assert(n != 0);
				if (n > 1) {
					curPat.resize(n);
					std::copy(i1, i1 + n, curPat.begin());

					bool add = true;
					for (const auto& prevPat : pats) {
						if (isSubpatternOf(curPat, prevPat)) {
							add = false;
							break;
						}
					}

					if (add) {
						pats.push_back(curPat);
					}

					curPat.clear();
				}
			}
		}
	}

	std::sort(pats.begin(), pats.end(), [] (const auto& a, const auto& b) {
		return b.size() > a.size();
	});

	std::vector<std::string> pats2;

	for (auto a = 0; a < pats.size(); a++) {
		bool add = true;
		const auto pat1 = pats[a];

		for (auto b = 0; b < pats.size(); b++) {
			if (a == b) continue;
			const auto pat2 = pats[b];

			if (isSubpatternOf(pat1, pat2)) {
				add = false;
				break;
			}
		}

		if (add) {
			pats2.push_back(pat1);
		}
	}

	std::reverse(pats2.begin(), pats2.end());
	return pats2;
}

std::optional<uint64_t> string_utils::fromStrToUInt64(
	const char* ptr,
	const std::string& alpha)
{
	const auto radix = alpha.size();
	uint64_t buf = 0;

	while(*ptr != '\0') {
		buf *= radix;

		char ch;

		if (std::isupper(*ptr)) {
			ch = static_cast<char>(std::tolower(*ptr));
		} else {
			ch = *ptr;
		}

		// can optimize to "ch - 0x30", etc
		auto idx = std::find(alpha.begin(), alpha.end(), ch);
		if (idx == alpha.end()) {
			return {};
		}

		size_t pos = idx - alpha.begin();
		buf += pos;
		ptr++;
	}

	return buf;
}
