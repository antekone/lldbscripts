#pragma once

#include <iostream>
#include <iomanip>
#include <sstream>

#define HD_ALIGN_DOWN(x)  ((x) & ~15)
#define HD_ALIGN_UP(x)    (HD_ALIGN_DOWN(x) + 15)

namespace HexDumperNS {

class CoutWriter {
public:
	void put(const char* str) {
		std::cout << str;
	}

	void put(const char ch) {
		char buf[2] = { ch, 0 };
		put(buf);
	}

	void pad(size_t len) {
		std::cout << std::setw(len);
	}
};

class OstringWriter {
private:
	std::stringstream sstr;

public:
	void put(const char* str) {
		sstr << str;
	}

	void put(const char ch) {
		char buf[2] = { ch, 0 };
		put(buf);
	}

	void pad(size_t len) {
		sstr << std::setw(len);
	}

	void make_string(std::string& out) {
		out = sstr.str();
	}
};

template <bool _want_offset,
	bool _want_header,
	typename TWriter>

class GenericStyle {
public:
	typedef TWriter Writer;
	static bool want_offset() { return _want_offset; }
	static bool want_header() { return _want_header; }
};

}

static const char* g_hextab = "0123456789ABCDEF";

template <typename TStyle>
class StyledHexDumper {
public:
	typedef TStyle Style;
	typedef typename TStyle::Writer Writer;

private:
	static uint64_t count_bchop(uint64_t n) {
		int r = 1;

		if(n < 0)
			n = (n == 0) ? UINT64_MAX : -n;

		if(n >= 100000000) {
			r += 8;
			n /= 100000000;
		}

		if(n >= 10000) {
			r += 4;
			n /= 10000;
		}

		if(n >= 100) {
			r += 2;
			n /= 100;
		}

		if(n >= 10)
			r++;

		return r;
	}

	static size_t calc_widelen(size_t size) {
		if(size > 15)
			return 16;

		return size;
	}

	static size_t calc_offsetlen(size_t offset, size_t size) {
		size_t n = count_bchop(offset + size);
		return n;
	}

	template <typename Writer>
	static void draw_header(Writer& wr, size_t offset, uint64_t size) {
		if(TStyle::want_offset()) {
			wr.pad(std::max<int>(9, calc_offsetlen(offset, size)));
			wr.put("    ");
			wr.pad(0);
			wr.put("  ");
		}

		bool mark = false;
		for(size_t i = 0; i < 16; ++i) {
			if(i > 7 && !mark) {
				wr.put(" -");
				mark = true;
			}

			wr.put(" 0");
			wr.put(g_hextab[i]);
		}

		wr.put("\n");

		if(TStyle::want_offset()) {
			wr.pad(std::max<int>(9, calc_offsetlen(offset, size)));
			wr.put("    ");
			wr.pad(0);
			wr.put("  ");
		}

		mark = false;
		for(size_t i = 0; i < 16; ++i) {
			if(i > 7 && !mark) {
				wr.put(" -");
				mark = true;
			}

			wr.put(" --");
		}

		wr.put("\n");
	}

	template <typename Writer, typename PointerType>
	static void draw_data(Writer& wr, PointerType* buf, size_t size, uint64_t offset) {
		bool offset_time = true;
		uint64_t aligned_start = HD_ALIGN_DOWN(offset);
		int offsetlen = std::max<int>(9, calc_offsetlen(aligned_start, size));
		int i = 0;
		bool mark = false;

		std::string asciicol;

		while(aligned_start < size + offset) {
			if(offset_time) {
				char buf2[32];
				sprintf(buf2, "%llx", (long long unsigned int) aligned_start);
				wr.pad(offsetlen);
				wr.put(buf2);
				wr.put("   ");
				offset_time = false;
				mark = false;
			}

			if(aligned_start >= offset) {
				char buf2[3] = { 0, 0, 0 };

				uint8_t b = buf[aligned_start - offset];
				buf2[0] = g_hextab[(b & 0xF0) >> 4];
				buf2[1] = g_hextab[b & 15];

				uint8_t ascii_b;
				if (std::isalnum(b) || std::isgraph(b)) {
					ascii_b = b;
				} else {
					ascii_b = '.';
				}

				asciicol.push_back(static_cast<char>(ascii_b));

				if(i > 7 && !mark) {
					wr.put("  ");
					mark = true;
				}

				wr.put(buf2);
				wr.put(" ");
			} else {
				asciicol.push_back(' ');
				wr.put("   ");
			}

			aligned_start++;
			i++;

			if(i >= 16) {
				wr.put("   ");
				wr.put(asciicol.c_str());
				asciicol.clear();
				wr.put("\n");
				offset_time = true;
				i = 0;
			}
		}
	}

public:
	template <typename Writer, typename PointerType>
	static void print(Writer& wr, PointerType* buf, size_t size, uint64_t offset) {
		if(TStyle::want_header()) {
			draw_header(wr, offset, size);
		}

		draw_data(wr, buf, size, offset);
		wr.put("\n");
	}
};

typedef StyledHexDumper<
	HexDumperNS::GenericStyle<
		true,
		false,
		HexDumperNS::CoutWriter
	>
> HexDumper;

#undef HD_ALIGN_UP
#undef HD_ALIGN_DOWN

