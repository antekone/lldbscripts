#pragma once
#include <vector>
#include <algorithm>
#include <string>
#include <optional>
#include <regex>
#include <gsl/gsl>
#include "../errors/command_error.h"

class NumberConversionAlphabets {
public:
	static const NumberConversionAlphabets& get() {
		static NumberConversionAlphabets alphas;
		return alphas;
	}

public:
	const std::string alphaHex = "0123456789abcdef";
	const std::string alphaBin = "01";
	const std::string alphaDec = "0123456789";
};

namespace string_utils {
	static std::vector<std::string> split(std::string const& str, char delim) {
		std::vector<std::string> tokens;

		size_t start;
		size_t end = 0;

		while ((start = str.find_first_not_of(delim, end)) != std::string::npos) {
			end = str.find(delim, start);
			tokens.push_back(str.substr(start, end - start));
		}

		return tokens;
	}

	std::optional<uint64_t> fromStrToUInt64(
		const char* ptr,
		const std::string& alpha
	);

	static std::optional<uint64_t> toUInt64(const std::string& in) {
		if (in.empty()) return {};

		if (in.size() > 2 && (in[0] == '0' && in[1] == 'x')) {
			auto m = std::find_if(in.begin() + 2, in.end(), [] (char ch) {
				if (std::isdigit(ch)) return false;
				if (ch >= 'a' && ch <= 'f') return false;
				if (ch >= 'A' && ch <= 'F') return false;
				return true;
			});

			if (m != in.end()) return {};
			return fromStrToUInt64(&in.c_str()[2],
								   NumberConversionAlphabets::get().alphaHex);
		}

		if (in.size() > 2 && (in[0] == '0' && in[1] == 'd')) {
			auto m = std::find_if(in.begin() + 2, in.end(), [] (char ch) {
				if (std::isdigit(ch)) return false;
				return true;
			});

			if (m != in.end()) return {};
			return fromStrToUInt64(&in.c_str()[2],
								   NumberConversionAlphabets::get().alphaDec);
		}

		if (in.size() > 2 && (in[0] == '0' && in[1] == 'b')) {
			auto m = std::find_if(in.begin() + 2, in.end(), [] (char ch) {
				if (ch == '0' || ch == '1') return false;
				return true;
			});

			if (m != in.end()) return {};
			return fromStrToUInt64(&in.c_str()[2],
								   NumberConversionAlphabets::get().alphaBin);
		}

		auto m = std::find_if(in.begin(), in.end(), [] (char ch) {
			return !std::isdigit(ch);
		});

		if (m != in.end()) return {};
		return fromStrToUInt64(in.c_str(),
							   NumberConversionAlphabets::get().alphaDec);
	}

	static uint64_t toUInt64OrThrow(const std::string& in) {
		auto r = toUInt64(in);
		if (!r) throw CommandError("invalid number: '{}'", in);
		return *r;
	}

	std::string interpolateSequence(
		const std::string& in,
		char seqCode,
		uint64_t seqValue
	);

	std::vector<std::string> findSubpatterns(
		const std::string& in1,
		const std::string& in2
	);

	static bool regexpMatch(const std::string& s, const std::optional<std::regex>& r) {
		std::smatch m;
		return std::regex_match(s, m, *r);
	}

	static bool substringMatch(std::string s, const std::optional<std::string>& query, bool ignoreCase) {
		if (ignoreCase) {
			std::transform(s.begin(), s.end(), s.begin(),
						   [] (auto c) { return std::tolower(c); });

		}
		return s.find(*query) != std::string::npos;
	}

	static std::string insertBitsetSpaces(const std::string& input) {
		if (input.size() < 64)
			return input;

		std::string output = input;
		for (gsl::index i = 1; i < 8; i++) {
			gsl::index dst = i * 8;
			output.insert(dst + i - 1, " ");
		}
		return output;
	}

	class AddressAndSize {
	public:
		uint64_t address = 0;
		size_t size = 0;
	};

	static 
	std::optional<AddressAndSize> parseAddressAndSize(const std::string& in) {
		auto parts = split(in, ':');
		if (parts.size() < 2)
			return {};

		auto addrNum = toUInt64(parts[0]);
		auto sizeNum = toUInt64(parts[1]);

		if (!addrNum || !sizeNum) 
			return {};

		if (*sizeNum > std::numeric_limits<size_t>::max())
			return {};

		return AddressAndSize { *addrNum, static_cast<size_t>(*sizeNum) };
	}
}