#pragma once

#include <concepts>
#include <optional>
#include <cxxopts.hpp>
#include <spdlog/spdlog.h>

#include "../utils/string_utils.h"
#include "../utils/output_adapter.h"

class CommandArgParser {
public:
	explicit CommandArgParser(
		const std::string& name,
		char** commands,
		const OutputAdapter& output
	):
		opts(name, fmt::format("Help for command '{}'", name)),
		adder(opts.add_options()),
		name(name),
		output(output)
	{
		args.push_back(name.c_str());

		if (commands != nullptr) {
			auto** ptr = commands;
			while (*ptr != nullptr) {
				args.push_back(*ptr);
				ptr++;
			}
		}

		args.push_back(nullptr);

		opts.custom_help("");
	}

	template <typename A>
	void addPositional(const std::string& name, const std::string& desc) {
		opts.parse_positional(name);
		adder(name, desc, cxxopts::value<A>());
	}

	template <typename A>
	void addPositional(const std::string& name, const std::string& desc, const std::string& help) {
		opts.parse_positional(name);
		opts.positional_help(help);
		adder(name, desc, cxxopts::value<A>(), help);
	}

	template <typename A>
	void addSwitch(const std::string& spec, const std::string& desc) {
		adder(spec, desc, cxxopts::value<A>());
	}

	void addSwitch(const std::string& spec, const std::string& desc) {
		adder(spec, desc);
	}

	bool parse() {
		addSwitch("h,help", "Display this help screen");

		int argc = static_cast<int>(args.size() - 1);
		char** argv =
			const_cast<char**>(reinterpret_cast<const char**>(args.data()));

		try {
			auto r = opts.parse(argc, argv);
			result.emplace(std::move(r));

			if (has("help")) {
				output.println(opts.help());
				return false;
			}

			return true;
		} catch (cxxopts::OptionException& e) {
			spdlog::error("Argument error ({}): {}", name, e.what());
			lastError = e.what();
			return false;
		}
	}

	[[nodiscard]] std::string usage() const {
		return opts.help();
	}

	[[nodiscard]] bool has(const std::string& spec) const {
		if (!stateOk()) return false;

		auto id = specToId(spec);
		return result->count(id) > 0;
	}

	template <typename A>
	std::optional<A> get(const std::string& spec) const {
		if (!stateOk()) return {};

		auto id = specToId(spec);
		if (result->count(id) > 0) {
			return (*result)[id].as<A>();
		} else {
			return {};
		}
	}

	template<typename A>
	A get(const std::string& spec, const A& def) const {
		if (!stateOk()) return def;

		auto r = get<A>(spec);
		if (!r) {
			return def;
		} else {
			return *r;
		}
	}

	std::string getLastError() const { return lastError; }


private:
	std::vector<const char*> args;
	cxxopts::Options opts;
	cxxopts::OptionAdder adder;
	std::optional<cxxopts::ParseResult> result;
	std::string name;
	std::string lastError;
	const OutputAdapter& output;

	bool stateOk() const {
		if (!result) return false;
		return true;
	}

	static std::string specToId(const std::string& spec) {
		std::string id;
		
		auto s = string_utils::split(spec, ',');
		if (s.empty() || s.size() < 2) {
			id = spec;
		} else {
			id = s[1];
		}

		return id;
	}
};

