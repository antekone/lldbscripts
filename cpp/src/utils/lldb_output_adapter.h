#pragma once

#include <string>

#include "output_adapter.h"

class LLDBOutputAdapter : public OutputAdapter {
public:
	explicit LLDBOutputAdapter(lldb::SBCommandReturnObject& ret): ret(ret)
	{}

	void printsln(const std::string& msg) const override {
		ret.Printf("%s\n", msg.c_str());
	}

	void prints(const std::string& msg) const override {
		ret.Printf("%s", msg.c_str());
	}

private:
	lldb::SBCommandReturnObject& ret;
};