#pragma once

#include <string>
#include <fmt/core.h>

class OutputAdapter {
public:
	virtual void printsln(const std::string& msg) const = 0;
	virtual void prints(const std::string& msg) const = 0;

	template <typename... Args>
	void print(const std::string& fmt, Args... args) const {
		auto msg = fmt::format(fmt::runtime(fmt), std::forward<Args>(args)...);
		prints(msg);
	}

	template <typename... Args>
	void println(const std::string& fmt, Args... args) const {
		auto msg = fmt::format(fmt::runtime(fmt), std::forward<Args>(args)...);
		printsln(msg);
	}
};
