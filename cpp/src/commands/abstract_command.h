#pragma once

#include <algorithm>
#include <array>

#ifndef HAVE_NEW_EXE_CTX_SIGNATURE
#include <regex>
#endif

#include <fmt/core.h>

#include <SBCommandInterpreter.h>
#include <SBCommandReturnObject.h>
#include <SBDebugger.h>
#include <SBTarget.h>
#include <SBProcess.h>
#include <SBThread.h>
#include <SBFrame.h>
#include <SBExecutionContext.h>

#include "../utils/command_arg_parser.h"
#include "../utils/lldb_output_adapter.h"
#include "../errors/command_error.h"
#include "../api/process.h"
#include "../api/frame.h"
#include "../api/target.h"

#include "config.h"

#ifdef HAVE_NEW_EXE_CTX_SIGNATURE
class AbstractCommand : public lldb::SBCommandPluginInterfaceWithContext {
#else
class AbstractCommand : public lldb::SBCommandPluginInterface {
#endif
public:
	virtual void args(CommandArgParser& ap) = 0;
	virtual bool run(const CommandArgParser& ap) = 0;
	virtual std::string name() = 0;

#ifdef HAVE_NEW_EXE_CTX_SIGNATURE
	bool DoExecute(lldb::SBDebugger debugger, char **commands, lldb::SBExecutionContext newCtx, lldb::SBCommandReturnObject& result) override {
#else
	bool DoExecute(lldb::SBDebugger debugger, char **commands, lldb::SBCommandReturnObject& result) override {
#endif

		dbg = &debugger;
		ret = &result;
		out.emplace(LLDBOutputAdapter(result));

#ifdef HAVE_NEW_EXE_CTX_SIGNATURE
        exeCtx = newCtx;
#else
		try {
			regenerateExecutionContext();
		} catch (const CommandError& e) {
			createEmptyExecutionContext();
		}
#endif

		CommandArgParser parser(name(), commands, LLDBOutputAdapter(result));
		args(parser);
		if (!parser.parse()) {
			spdlog::error("Error when parsing arguments");
			auto lastError = parser.getLastError();
			if (!lastError.empty()) {
				result.SetError(lastError.c_str());
				return false;
			}

			return true;
		}

		try {
			err = lldb::SBError();
			auto retv = run(parser);
			if (!retv) {
				if (result.GetErrorSize() == 0) {
					result.SetError("Command returned failure.");
				}

				spdlog::trace("Returning false");
				return false;
			}
		} catch (const CommandError& e) {
			spdlog::error("Got CommandError exception: {}", e.what());
			auto msg = fmt::format("{}", e.what());
			result.SetError(msg.c_str());
			return false;
		} catch (const fmt::format_error& e) {
			spdlog::error("Got FormatError exception: {}", e.what());
			auto msg = fmt::format("(internal formatting error: {})", e.what());
			result.SetError(msg.c_str());
			return false;
		}

		ret->SetStatus(lldb::ReturnStatus::eReturnStatusSuccessFinishNoResult);
		return true;
	}

protected:
	lldb::SBDebugger* dbg;
	lldb::SBCommandReturnObject* ret;
	lldb::SBError err;
	lldb::SBExecutionContext exeCtx;
	std::optional<LLDBOutputAdapter> out;

	template <typename... Args>
	void throwIfError(const std::string& fmt, Args... args) const {
		if (err) {
			throw CommandError(err, fmt, std::forward<Args>(args)...);
		}
	}

	template <typename... Args>
	void error(const std::string& fmt, Args... args) const {
		auto msg = fmt::format(fmt, std::forward<Args>(args)...);
		ret->SetError(msg.c_str());
	}

	template <typename... Args>
	void println(const std::string& fmt, Args... args) const {
		out->println(fmt, std::forward<Args>(args)...);
	}

	template <typename... Args>
	void print(const std::string& fmt, Args... args) const {
		out->print(fmt, std::forward<Args>(args)...);
	}

	PProcess currentProcess() {
		auto proc = exeCtx.GetProcess();
		if (!proc.IsValid()) throw CommandError("invalid process");
		return PProcess(proc);
	}

	PFrame currentFrame() {
		auto frame = exeCtx.GetFrame();
		if (!frame.IsValid()) throw CommandError("invalid frame");
		return PFrame(frame);
	}

	PTarget currentTarget() {
		auto tgt = exeCtx.GetTarget();
		if (!tgt.IsValid()) throw CommandError("invalid target");
		return PTarget(tgt);
	}

private:

	void createEmptyExecutionContext() {
		auto tgt = dbg->GetSelectedTarget();
		if (tgt.IsValid()) {
			auto proc = tgt.GetProcess();
			if (proc.IsValid()) {
				// That's the maximum we can get in this context.
				exeCtx = lldb::SBExecutionContext(proc);
			} else {
				exeCtx = lldb::SBExecutionContext(tgt);
			}
		} else {
			exeCtx = lldb::SBExecutionContext();
		}
	}

#ifndef HAVE_NEW_EXE_CTX_SIGNATURE
	void regenerateExecutionContext() {
		// Hard to swallow the fact that Python API is more powerful than the
		// C++ API ;(
		//
		// https://discourse.llvm.org/t/sbprocess-getselectedthread-inside-the-breakpoint-callback-context/62421

		auto interp = dbg->GetCommandInterpreter();

		lldb::SBCommandReturnObject result;
		auto succ = interp.HandleCommand("thread info", result, false);

		static std::array<lldb::ReturnStatus, 4> acceptableReturnCodes = {
			lldb::ReturnStatus::eReturnStatusSuccessFinishNoResult,
			lldb::ReturnStatus::eReturnStatusSuccessFinishResult,
			lldb::ReturnStatus::eReturnStatusSuccessContinuingNoResult,
			lldb::ReturnStatus::eReturnStatusSuccessContinuingResult,
		};

		if (std::find(acceptableReturnCodes.begin(),
					  acceptableReturnCodes.end(),
					  succ) == acceptableReturnCodes.end())
		{
			throw CommandError("unable to create execution context, error 1");
		}

		if (result.GetOutput() == nullptr || result.GetOutputSize() < 5) {
			spdlog::error("result.GetOutput() is invalid");
			throw CommandError("unable to create execution context, error 2");
		}

		static std::regex tid("^thread .*?: tid = (.*?), .*");
		std::smatch m;
		std::string s = result.GetOutput();

		if (std::regex_search(s, m, tid)) {
			const auto threadIdStr = m[1].str();
//			spdlog::trace("Got thread id: {}", threadIdStr);
			regenerateExecutionContextFromThreadID(
				string_utils::toUInt64OrThrow(threadIdStr));
		} else {
			spdlog::error("can't match 'frame info' output: >>> {} <<<", s);
			throw CommandError("unable to create execution context, error 3");
		}
	}

	void regenerateExecutionContextFromThreadID(uint32_t threadId) {
		auto tgt = dbg->GetSelectedTarget();
		if (tgt.IsValid()) {
			auto proc = tgt.GetProcess();
			if (proc.IsValid()) {
				// GetSelectedThread() is buggy -- doesn't work in the
				// context of a breakpoint callback
				auto thread = proc.GetThreadByID(threadId);
				if (thread.IsValid()) {
					auto frame = thread.GetSelectedFrame();
					if (frame.IsValid()) {
						exeCtx = lldb::SBExecutionContext(frame);
					} else {
						exeCtx = lldb::SBExecutionContext(thread);
					}
				} else {
					exeCtx = lldb::SBExecutionContext(proc);
				}
			} else {
				exeCtx = lldb::SBExecutionContext(tgt);
			}
		} else {
			exeCtx = lldb::SBExecutionContext();
		}
	}
#endif
};
