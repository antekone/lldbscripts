#pragma once

#include "abstract_command.h"

class DumpHexCommand : public AbstractCommand {
private:

public:
	void args(CommandArgParser& ap) override;
	bool run(const CommandArgParser& ap) override;

	std::string name() override { return "dump-hex"; }
};
