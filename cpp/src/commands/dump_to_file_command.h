#pragma once

#include <map>
#include <string>

#include "abstract_command.h"

class DumpToFileCommand : public AbstractCommand {
public:
	void args(CommandArgParser& ap) override;
	bool run(const CommandArgParser& ap) override;

	std::string name() override { return "dump-to-file"; }

	uint64_t querySequenceValue(const std::string& name) {
		return sequences[name];
	}

	void setSequenceValue(const std::string& name, uint64_t value) {
		sequences[name] = value;
	}

	std::string interpolate(const std::string& in, const CommandArgParser& ap);

	void incrementSequences(const CommandArgParser& ap);
	void processGetSequences(const CommandArgParser& ap);
	void processSetSequences(const CommandArgParser& ap);
	void processKillSequences(const CommandArgParser& ap);
	void processDump(const CommandArgParser& ap);

private:
	std::map<std::string, uint64_t> sequences;
};