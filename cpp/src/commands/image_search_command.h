#pragma once

#include "abstract_command.h"

#include <SBModule.h>

class ImageSearchCommand : public AbstractCommand {
	void args(CommandArgParser& ap) override;
	bool run(const CommandArgParser& ap) override;
	std::string name() override { return "image-search"; }
private:
	bool doRun(const CommandArgParser& ap);
	void dumpVerbose(lldb::SBTarget tgt, lldb::SBModule m);
	void dumpSections(lldb::SBTarget tgt, lldb::SBModule m);
};