#pragma once

#include "abstract_command.h"

class SymbolSearchCommand : public AbstractCommand {
public:
	void args(CommandArgParser& ap) override;
	bool run(const CommandArgParser& ap) override;
	std::string name() override { return "symbol-search"; }
private:
	bool doRun(const CommandArgParser& ap);
};