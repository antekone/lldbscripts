#pragma once

#include "abstract_command.h"

class BitCompareCommand : public AbstractCommand {
public:
	void args(CommandArgParser& ap) override;
	bool run(const CommandArgParser& ap) override;
	std::string name() override { return "bit-compare"; }
};