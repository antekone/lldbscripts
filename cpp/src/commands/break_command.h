#pragma once

#include "abstract_command.h"

enum class BreakpointType {
    Normal, EqualsData, Counter
};

class BreakpointSpec {
public:
    BreakpointType type;
    template <typename... Args>
    static std::shared_ptr<BreakpointSpec> create(Args&&... args) {
        return std::make_shared<BreakpointSpec>(std::forward<Args>(args)...);
    }

};
using BreakpointSpecRef = std::shared_ptr<BreakpointSpec>;

class BreakCommand : public AbstractCommand {
public:
    void args(CommandArgParser& ap) override;
    bool run(const CommandArgParser& ap) override;
};