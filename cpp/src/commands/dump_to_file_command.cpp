#include <gsl/gsl>
#include <cstdio>
#include <algorithm>
#include <memory>
#include "../errors/command_error.h"
#include "dump_to_file_command.h"

using SafeHandle = std::unique_ptr<FILE, std::function<void (FILE*)>>;

void DumpToFileCommand::args(CommandArgParser& ap) {
	ap.addPositional<std::vector<std::string>>("addresses",
		"The addresses 'from to' from which dumping should start");

	ap.addSwitch<std::string>("f,fmt",
		"Format to dump: bin, hex, c. Default: bin");

	ap.addSwitch<std::string>("o,out",
		"Output file path.");

	ap.addSwitch<std::string>("q,fail-guard",
		"Fail early if the region size is bigger than <arg>.");

	ap.addSwitch<std::vector<std::string>>("s,set",
		"Set sequence <arg> to a specified value. If no argument is provided, "
		"then set this sequence to 0. E.g. -s X=10");

	ap.addSwitch<std::vector<std::string>>("g,get",
		"Print sequence <arg>'s value.");

	ap.addSwitch<std::vector<std::string>>("k,kill",
		"Kill sequence <arg> (i.e. forget about it).");

	ap.addSwitch<std::vector<std::string>>("i,inc",
		"Increment this sequence by 1. E.g. -i X. Incrementation will be "
		"done only when the memory reading operation succeeds.");

	ap.addSwitch("n,dry-run",
	    "Perform only dry run (i.e. don't perform any actual memory dumping), "
		"useful to just operate on sequences.");

	ap.addSwitch("d,dynamic",
		"Treat 'X' letters in the -o filename as a template for sequence X "
		"(e.g. '/tmp/fileXXXX.dat' will be interpreted as /tmp/file0030.dat' "
		"if sequence X has a value of 30). If there is not enough room for "
		"the number, it will not overflow, so one 'X' is enough if you don't "
		"want any padding.");
}

SafeHandle getHandle(const std::string& fn, const std::string& mode) {
	auto fp = SafeHandle(::fopen(fn.c_str(), mode.c_str()),
						 [] (FILE* f) { ::fclose(f); });

	if (!fp) throw CommandError("can't open path for writing: {}", fn);
	return fp;
}

size_t saveBinToFile(
	const std::vector<uint8_t>& buf,
	const std::string& filename
) {
	auto fp = getHandle(filename, "wb");

	return buf.size() * ::fwrite(buf.data(), buf.size(), 1, fp.get());
}

size_t saveHexToFile(
	const std::vector<uint8_t>& buf,
	const std::string& filename
) {
	auto fp = getHandle(filename, "wt");

	size_t written = 0;
	std::array<char, 3> tempBuf = {};
	for (const auto b : buf) {
		std::fill(tempBuf.begin(), tempBuf.end(), 0);
		::snprintf(tempBuf.data(), tempBuf.size(), "%02x", b);

		auto len = ::strlen(tempBuf.data());
		written += len * ::fwrite(tempBuf.data(), len, 1, fp.get());
	}

	return written;
}

size_t saveCToFile(
	const std::vector<uint8_t>& buf,
	const std::string& filename
) {
	auto fp = getHandle(filename, "wt");

	size_t written = 0;
	size_t count = 0;
	size_t lastElement = buf.size() - 1;

	std::array<char, 7> tempBuf = {};
	for (const auto b : buf) {
		std::fill(tempBuf.begin(), tempBuf.end(), 0);

		if (count == lastElement) {
			::snprintf(tempBuf.data(), tempBuf.size(), "0x%02x", b);
		} else {
			::snprintf(tempBuf.data(), tempBuf.size(), "0x%02x,", b);
		}

		auto len = ::strlen(tempBuf.data());
		written += len * ::fwrite(tempBuf.data(), len, 1, fp.get());
		count++;
	}

	return written;
}

void DumpToFileCommand::processKillSequences(const CommandArgParser& ap) {
	const auto args = ap.get<std::vector<std::string>>("kill");
	if (!args) return;

	for (const auto& seq : *args) {
		sequences.erase(seq);
		println("Killed sequence '{}'.", seq);
	}
}

void DumpToFileCommand::incrementSequences(const CommandArgParser& ap) {
	const auto args = ap.get<std::vector<std::string>>("inc");
	if (!args) return;

	for (const auto& seq : *args) {
		auto curValue = querySequenceValue(seq);
		setSequenceValue(seq, curValue + 1);
		println("Incremented sequence '{}' from {} to {}", seq, curValue,
				curValue + 1);
	}
}

std::string DumpToFileCommand::interpolate(
	const std::string& in, const CommandArgParser& ap
) {
	if (!ap.has("dynamic")) return in;

	std::string out = in;

	for (const auto& [seqCode, seqValue] : sequences) {
		out = string_utils::interpolateSequence(out, seqCode[0], seqValue);
	}

	return out;
}


void DumpToFileCommand::processGetSequences(const CommandArgParser& ap) {
	const auto args = ap.get<std::vector<std::string>>("get");
	if (!args) return;

	if (args->empty() || (*args)[0] == "all") {
		for (const auto& [name, value] : sequences) {
			println("Value of sequence '{}': 0x{:08X}, {}d",
					name, value, value);
		}
	} else {
		for (const auto& seqCode: *args) {
			if (sequences.contains(seqCode)) {
				const auto value = querySequenceValue(seqCode);
				println("Value of sequence '{}': 0x{:08X}, {}d",
						seqCode, value, value);
			} else {
				println("Sequence '{}' not found.", seqCode);
			}
		}
	}
}

void DumpToFileCommand::processSetSequences(const CommandArgParser& ap) {
	const auto args = ap.get<std::vector<std::string>>("set");
	if (!args) return;

	for (const auto& seqPair: *args) {
		const auto tokens = string_utils::split(seqPair, '=');
		if (tokens.size() != 2)
			throw CommandError("invalid syntax for -s: expected X=<value>, "
							   "instead got '{}'", seqPair);

		if (tokens[0].size() != 1)
			throw CommandError("sequence name should be just 1 char, instead "
							   "got '{}'", tokens[0]);

		auto num = string_utils::toUInt64(tokens[1]);
		if (num) {
			setSequenceValue(tokens[0], *num);
			println("Settings value of sequence '{}' to 0x{:08X}, {}d",
					tokens[0], *num, *num);
		} else {
			// Try it again, but this time evaluate the number using current
			// frame's expression parser.
			try {
				auto frame = currentFrame();
				auto num2 = frame.evalToUInt64(tokens[1]);
				setSequenceValue(tokens[0], num2);
			} catch (const CommandError& e) {
				throw CommandError("no frame, so can't evaluate the argument "
								   "('{}')", tokens[1]);
			}
		}
	}
}

void DumpToFileCommand::processDump(const CommandArgParser& ap) {
	bool performDump = !ap.has("dry-run");
	if (performDump) {
		auto format = ap.get<std::string>("fmt", "bin");
		auto addresses = ap.get<std::vector<std::string>>("addresses");

		if (!addresses || addresses->size() != 2)
			throw CommandError("expecting 'from' and 'to' addresses in the "
							   "command-line");

		auto frame = currentFrame();

		auto addrFrom = frame.evalToUInt64((*addresses)[0]);
		auto addrTo = frame.evalToUInt64((*addresses)[1]);

		if (addrTo <= addrFrom)
			throw CommandError("destination address is smaller or equal to "
							   "source address");

		auto guard = ap.get<std::string>("fail-guard");
		if (guard) {
			auto size = addrTo - addrFrom;
			auto guardNum = string_utils::toUInt64OrThrow(*guard);
			if (size > guardNum) {
				throw CommandError(
					"size (0x{:08X}) is greater than the guard argument "
					"(0x{:08X}), aborting", size, guardNum);
			}
		}

		auto proc = currentProcess();
		auto buf = proc.queryMemory(addrFrom, addrTo - addrFrom);
		std::string fn;
		size_t written;

		spdlog::trace("Will dump");
		if (format == "bin") {
			spdlog::trace("Will dump bin");
			fn = ap.get<std::string>("out", "/tmp/memory-dump.bin");
			fn = interpolate(fn, ap);
			written = saveBinToFile(buf, fn);
		} else if (format == "c") {
			fn = ap.get<std::string>("out", "/tmp/memory-dump.c");
			fn = interpolate(fn, ap);
			written = saveCToFile(buf, fn);
		} else if (format == "hex") {
			fn = ap.get<std::string>("out", "/tmp/memory-dump.hex");
			fn = interpolate(fn, ap);
			written = saveHexToFile(buf, fn);
		} else {
			throw CommandError("unknown format");
		}

		spdlog::trace("Dump done");
		println("Written {} bytes (format {}) to {}", written, format, fn);
	}
}

bool DumpToFileCommand::run(const CommandArgParser& ap) {
	processGetSequences(ap);
	processSetSequences(ap);
	processKillSequences(ap);
	processDump(ap);
	incrementSequences(ap);
	return true;
}
