#include <spdlog/spdlog.h>

#include "../utils/hex_dumper.h"
#include "../utils/command_arg_parser.h"
#include "../api/frame.h"
#include "../api/process.h"

#include "dump_hex_command.h"

static const std::string ARG_POSITIONAL = "positional";
static const std::string ARG_NUMBER = "n,number";

class LLDBWriter {
public:
	explicit LLDBWriter(OutputAdapter& out) : out(out) {}

	void put(const char* str) {
		out.print(str);
	}

	void put(const char ch) {
		char buf[2] = { ch, 0 };
		put(buf);
	}

	void pad(size_t len) {
	}

private:
	OutputAdapter& out;
};

void DumpHexCommand::args(CommandArgParser& ap) {
	ap.addPositional<std::string>("positional", "Address to dump");
	ap.addSwitch<std::string>("l,lines", "Number of lines to display (default 5) (exclusive with --bytes)");
	ap.addSwitch<std::string>("b,bytes", "Number of bytes to display (default 80) (exclusive with --lines)");
}

bool DumpHexCommand::run(const CommandArgParser& ap) {
	auto lines = ap.get<std::string>("lines");
	auto bytes = ap.get<std::string>("bytes");
	auto expr = ap.get<std::string>("positional");

	if (!expr)
		throw CommandError("missing main argument");

	if (lines && bytes)
		throw CommandError("--lines and --bytes are mutually exclusive.");

	auto frame = currentFrame();

	size_t size = 80;
	if (lines) {
		size = 16 * frame.evalToUInt64(*lines);
	} else if (bytes) {
		size = frame.evalToUInt64(*bytes);
	}

	auto addr = frame.evalToUInt64(*expr);
	auto endAddr = addr + size - 1;
	if (endAddr <= addr || size > 1 * 1024 * 1024)
		throw CommandError("invalid range (negative or too large)");

	spdlog::trace("Argument parsing done");

	auto proc = currentProcess();
	auto buf = proc.queryMemory(addr, size);

	spdlog::trace("Memory reading done");

	HexDumperNS::OstringWriter wr;
	HexDumper::print(wr, buf.data(), buf.size(), addr);

	std::string outString;
	wr.make_string(outString);
	print("{}", outString);

	return true;
}