#pragma once

#include "abstract_command.h"

class PrintCommand : public AbstractCommand {
public:
	void args(CommandArgParser& ap) override;
	bool run(const CommandArgParser& ap) override;
	std::string name() override { return "print"; };
private:
};