#include <algorithm>
#include <regex>
#include <gsl/gsl>

#include <SBSymbol.h>

#include "symbol_search_command.h"

#include "../api/file_spec.h"
#include "../utils/string_utils.h"

const static char* ARG_POSITIONAL = "positional";
const static char* ARG_IGNORE_CASE = "i,ignore-case";
const static char* ARG_REGEXP = "r,regexp";
const static char* ARG_VERBOSE = "v,verbose";

void SymbolSearchCommand::args(CommandArgParser& ap) {
	ap.addPositional<std::string>(ARG_POSITIONAL, "name of the symbol to search for");
	ap.addSwitch(ARG_REGEXP, "argument is a regexp instead of a substring");
	ap.addSwitch(ARG_IGNORE_CASE, "ignore case when searching");
	ap.addSwitch(ARG_VERBOSE, "print more information about each image");
}

bool SymbolSearchCommand::run(const CommandArgParser& ap) {
	try {
		return doRun(ap);
	} catch (std::regex_error& e) {
		throw CommandError("invalid regexp: {}", e.what());
	}
}

bool SymbolSearchCommand::doRun(const CommandArgParser& ap) {
	auto query = ap.get<std::string>(ARG_POSITIONAL, "");
	auto ignoreCase = ap.has(ARG_IGNORE_CASE);
	auto hasRegexp = ap.has(ARG_REGEXP);
	auto hasVerbose = ap.has(ARG_VERBOSE);

	std::optional<std::regex> queryRegexp = {};
	std::optional<std::string> queryString = {};

	if (hasRegexp) {
		auto flags = std::regex_constants::optimize;
		if (ignoreCase)
			flags |= std::regex_constants::icase;
		queryRegexp = std::regex(query, flags);
	} else {
		if (ignoreCase) {
			std::transform(query.begin(), query.end(), query.begin(),
						   [] (auto c) { return std::tolower(c); });
		}
		queryString = query;
	}

	auto tgt = currentTarget().raw();
	auto count = tgt.GetNumModules();
	for (auto i = 0; i < count; i++) {
		auto module = tgt.GetModuleAtIndex(i);
		auto fileSpec = module.GetFileSpec();
		gsl::czstring fileName = fileSpec.GetFilename();

		size_t symCount = module.GetNumSymbols();
		for (auto k = 0; k < symCount; k++) {
			auto sym = module.GetSymbolAtIndex(k);
			auto addr = sym.GetStartAddress();

			std::string symName;
			gsl::czstring displayName = sym.GetDisplayName();
			if (displayName) {
				symName = displayName;
			} else {
				symName = "(null name)";
			}

			std::string fullSymName = fmt::format("{}`{}", fileName, symName);

			int hit = 0;
			hit += hasRegexp && string_utils::regexpMatch(fullSymName, queryRegexp);
			hit += !hasRegexp && string_utils::substringMatch(fullSymName, queryString, ignoreCase);
			if (!hit) continue;

			println("0x{:016X} -- {}", addr.GetLoadAddress(tgt), fullSymName);

		}
	}

	return true;
}
