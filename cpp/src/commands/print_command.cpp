#include <string>
#include <gsl/gsl>

#include "../errors/command_error.h"

#include "print_command.h"

static const std::string ARG_POSITIONAL = "positional";
static const std::string ARG_TYPE = "t,type";

void PrintCommand::args(CommandArgParser& ap) {
	ap.addPositional<std::string>(ARG_POSITIONAL,
		"<expr>");
	ap.addSwitch<std::string>(ARG_TYPE,
		"b2/base2");
}

bool PrintCommand::run(const CommandArgParser& ap) {
	const auto e = ap.get<std::string>(ARG_POSITIONAL);
	if (!e) {
		throw CommandError("missing positional argument");
	}

	auto f = currentFrame();
	auto value = f.evalToUInt64(*e);

	const auto maybeType = ap.get<std::string>(ARG_TYPE);
	if (maybeType) {
		const auto type = *maybeType;
		if (type == "b2" || type == "base2") {
			println("  - 87654321 87654321 87654321 87654321 87654321 87654321 87654321 87654321");
			println("  + 12345678 12345678 12345678 12345678 12345678 12345678 12345678 12345678");
			auto s = std::bitset<64>(value).to_string();
			println("--> {}", string_utils::insertBitsetSpaces(s));
		} else {
			throw CommandError("unknown type: '{}'", type);
		}
	} else {
		println("{}", value);
	}

	return true;
}
