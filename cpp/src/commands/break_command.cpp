#include "break_command.h"

static const char* ARG_CODE_ADDR = "code-addr";
static const char* ARG_DATA_ADDR = "data-addr";
static const char* ARG_DATA_EQUALS = "data-equals";
static const char* ARG_COUNTER_EQUALS = "counter-equals";

std::vector<BreakpointSpecRef> probeList;

void BreakCommand::args(CommandArgParser& ap) {

}

bool BreakCommand::run(const CommandArgParser& ap) {

}
