#include <algorithm>
#include <regex>
#include <gsl/gsl>

#include <SBModule.h>

#include "image_search_command.h"

#include "../api/file_spec.h"
#include "../utils/string_utils.h"

static const char* ARG_POSITIONAL = "positional";
static const char* ARG_REGEXP = "r,regexp";
static const char* ARG_IGNORE_CASE = "i,ignore-case";
static const char* ARG_VERBOSE = "v,verbose";

void ImageSearchCommand::args(CommandArgParser& ap) {
	ap.addPositional<std::string>(ARG_POSITIONAL, "name of an image to search for");
	ap.addSwitch(ARG_REGEXP, "argument is a regexp instead of a substring");
	ap.addSwitch(ARG_IGNORE_CASE, "ignore case when searching");
	ap.addSwitch(ARG_VERBOSE, "print more information about each image");
}

bool ImageSearchCommand::run(const CommandArgParser& ap) {
	try {
		return doRun(ap);
	} catch (std::regex_error& e) {
		throw CommandError("invalid regexp: {}", e.what());
	}
}

bool ImageSearchCommand::doRun(const CommandArgParser& ap) {
	auto query = ap.get<std::string>(ARG_POSITIONAL, "");
	auto ignoreCase = ap.has(ARG_IGNORE_CASE);
	auto hasRegexp = ap.has(ARG_REGEXP);
	auto hasVerbose = ap.has(ARG_VERBOSE);

	std::optional<std::regex> queryRegexp = {};
	std::optional<std::string> queryString = {};

	if (hasRegexp) {
		auto flags = std::regex_constants::optimize;
		if (ignoreCase)
			flags |= std::regex_constants::icase;
		queryRegexp = std::regex(query, flags);
	} else {
		if (ignoreCase) {
			std::transform(query.begin(), query.end(), query.begin(),
						   [] (auto c) { return std::tolower(c); });
		}
		queryString = query;
	}

	auto tgt = currentTarget().raw();
	auto count = tgt.GetNumModules();
	for (auto i = 0; i < count; i++) {
		auto module = tgt.GetModuleAtIndex(i);
		auto path = PFileSpec(module.GetPlatformFileSpec());
		auto pathStr = path.path();

		int hit = 0;
		hit += queryRegexp && string_utils::regexpMatch(pathStr, queryRegexp);
		hit += queryString && string_utils::substringMatch(pathStr,
			queryString, ignoreCase);
		if (!hit) continue;

		auto fileHeaderAddr = module.GetSectionAtIndex(0).GetLoadAddress(tgt);

		println("0x{:016X} -- {}", fileHeaderAddr, pathStr);

		if (hasVerbose) {
			dumpVerbose(tgt, module);
		}
	}

	return true;
}

void ImageSearchCommand::dumpVerbose(lldb::SBTarget tgt, lldb::SBModule m) {
	dumpSections(tgt, m);
}

void ImageSearchCommand::dumpSections(lldb::SBTarget tgt, lldb::SBModule m) {
	for (size_t i = 0, n = m.GetNumSections(); i < n; ++i) {
		auto sect = m.GetSectionAtIndex(i);
		gsl::czstring name = sect.GetName();
		auto startOffset = sect.GetLoadAddress(tgt);
		auto physOffset = sect.GetFileOffset();
		auto perm = sect.GetPermissions();

		std::string permStr;

		if (perm & lldb::Permissions::ePermissionsReadable)
			permStr += "R";
		else
			permStr += "-";

		if (perm & lldb::Permissions::ePermissionsWritable)
			permStr += "W";
		else
			permStr += "-";

		if (perm & lldb::Permissions::ePermissionsExecutable)
			permStr += "X";
		else
			permStr += "-";

		println("    {:24} {} load 0x{:016X}, phys 0x{:016X}",
				name, permStr, startOffset, physOffset);
	}
}