#include <bitset>
#include <vector>
#include <string>
#include <gsl/gsl>

#include "bit_compare_command.h"

static const char* ARG_POSITIONAL = "positional";

void BitCompareCommand::args(CommandArgParser& ap) {
	ap.addPositional<std::vector<std::string>>(ARG_POSITIONAL, "two bitfields to compare");
}

bool BitCompareCommand::run(const CommandArgParser& ap) {
	const auto& args = ap.get<std::vector<std::string>>(ARG_POSITIONAL);
	if (!args || args->size() < 2) {
		throw CommandError("missing two arguments");
	}

	auto bf1 = string_utils::toUInt64((*args)[0]);
	auto bf2 = string_utils::toUInt64((*args)[1]);

	if (!bf1) {
		bf1 = currentFrame().evalToUInt64((*args)[0]);
	}

	if (!bf2) {
		bf2 = currentFrame().evalToUInt64((*args)[1]);
	}

	if (!bf1 || !bf2)
		throw CommandError("unknown argument");

	auto bin1 = std::bitset<64>(*bf1).to_string();
	auto bin2 = std::bitset<64>(*bf2).to_string();

	const auto subpatterns = string_utils::findSubpatterns(bin1, bin2);

	std::string out1;
	std::string out2;

	out1.resize(bin1.size(), ' ');
	out2.resize(bin2.size(), ' ');

	static const std::string subpatternAlpha =
		"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz!@#$%^&*()-=+_[]{};:<>?,./`~";

	if (subpatterns.size() > subpatternAlpha.size()) {
		throw CommandError("too many subpatterns, sorry");
	}

	for (size_t subpatternIdx = 0, len = subpatterns.size(); subpatternIdx < len; subpatternIdx++) {
		const auto& subpattern = subpatterns[subpatternIdx];
		const auto idx = bin1.find(subpattern);
		if (idx == std::string::npos) {
			throw CommandError("internal error in findSubpatterns()");
		}

		for (auto i = 0; i < subpattern.size(); i++) {
			out1[idx + i] = subpatternAlpha[subpatternIdx];
		}
	}

	for (size_t subpatternIdx = 0, len = subpatterns.size(); subpatternIdx < len; subpatternIdx++) {
		const auto& subpattern = subpatterns[subpatternIdx];
		const auto idx = bin2.find(subpattern);
		if (idx == std::string::npos) {
			throw CommandError("internal error in findSubpatterns()");
		}

		for (auto i = 0; i < subpattern.size(); i++) {
			out2[idx + i] = subpatternAlpha[subpatternIdx];
		}
	}

	println("First:  0x{:016x}", *bf1);
	println("First:  {}", string_utils::insertBitsetSpaces(bin1));
	println("        {}", string_utils::insertBitsetSpaces(out1));
	println("");
	println("Second: 0x{:016x}", *bf2);
	println("Second: {}", string_utils::insertBitsetSpaces(bin2));
	println("        {}", string_utils::insertBitsetSpaces(out2));

	println("Subpatterns:");
	for (size_t i = 0; i < subpatterns.size(); i++) {
		println("{} -- {}", subpatternAlpha[i], subpatterns[i]);
	}

	return true;
}
