#include <algorithm>
#include <optional>
#include <SBAddress.h>
#include "probe_command.h"
#include "../api/module.h"

const static char* ARG_NAME = "name";
const static char* ARG_CODE_ADDR = "code-addr";
const static char* ARG_DATA_ADDR = "data-addr";
const static char* ARG_DATA_SIZE = "data-size";
const static char* ARG_FILENAME = "filename";
const static char* ARG_DRY_RUN = "dry-run";
const static char* ARG_RESYNC = "resync";
const static char* ARG_LIST = "list";
const static char* ARG_CLEAR = "clear";
const static char* ARG_REMOVE = "remove";

std::vector<ProbeSpecRef> probeList;

void ProbeCommand::args(CommandArgParser& ap) {
    ap.addSwitch(ARG_LIST, "list current probes");
    ap.addSwitch<std::string>(ARG_REMOVE, "remove a named probe");
    ap.addSwitch(ARG_CLEAR, "clear all probes");
    ap.addSwitch(ARG_RESYNC, "resync all missing probes (set needed " \
        "breakpoints)");

    ap.addSwitch<std::string>(ARG_NAME, "name of the probe");
    ap.addSwitch<std::string>(ARG_CODE_ADDR, "code address evaluation string " \
        "(e.g. \"$x0\")");
    ap.addSwitch<std::string>(ARG_DATA_ADDR, "data address evaluation string " \
        "(e.g. \"$x15\")");
    ap.addSwitch<std::string>(ARG_DATA_SIZE, "data size evaluation string " \
        "(e.g. \"$x1\")");
    ap.addSwitch<std::string>(ARG_FILENAME, "template name of the file to " \
        "receive the data dump (e.g. /tmp/mvs-in-XXXX.bin)");
    ap.addSwitch(ARG_DRY_RUN, "perform a dry run -- just print the events on " \
        "the console");
}

bool ProbeCommand::run(const CommandArgParser& ap) {
    try {
        if (ap.has(ARG_LIST)) {
            return doList(ap);
        } else if (ap.has(ARG_CLEAR)) {
            return doClear(ap);
        } else if (ap.has(ARG_RESYNC)) {
            return doResync(ap);
        } else if (ap.has(ARG_REMOVE)) {
            return doRemove(ap);
        } else if (ap.has(ARG_NAME)) {
            return doInstall(ap);
        } else {
            throw CommandError("invalid args, try --help");
        }
    } catch (std::exception& e) {
        throw CommandError("other error: {}", e.what());
    }
}

extern size_t saveBinToFile(
    const std::vector<uint8_t>& buf,
    const std::string& filename
);

bool typedBreakpointCallback(
    ProbeSpec* probe,
    lldb::SBProcess& proc,
    lldb::SBThread& thread,
    lldb::SBBreakpointLocation& loc
) {
    auto frame = PFrame(thread.GetSelectedFrame());
    auto dataAddr = frame.evalToUInt64(probe->dataAddrString);
    auto dataSize = frame.evalToUInt64(probe->dataSizeString);

    auto filename = string_utils::interpolateSequence(
        probe->filenameTemplate, 'X', probe->hitCount);

    spdlog::trace("{}: will dump address 0x{:08x} (size 0x{:08x}) to '{}'",
                  probe->name, dataAddr, dataSize, filename);

    if (!probe->dryRun) {
        auto p = PProcess(proc);
        auto mem = p.queryMemory(dataAddr, dataSize);
        saveBinToFile(mem, filename);
    }

    probe->hitCount++;
    return true;
}

template <typename Arg>
bool breakpointCallback(
    void* arg,
    lldb::SBProcess& proc,
    lldb::SBThread& thread,
    lldb::SBBreakpointLocation& loc
) {
    try {
        return typedBreakpointCallback(
            reinterpret_cast<Arg*>(arg), proc, thread, loc);
    } catch (const CommandError& e) {
        spdlog::error("error during evaluation of a breakpoint callback "\
            "function: {}", e.what());
        return false;
    }
}

bool ProbeCommand::doInstall(const CommandArgParser& ap) {
    std::vector<std::string> required = {
        ARG_NAME, ARG_CODE_ADDR, ARG_DATA_ADDR, ARG_FILENAME
    };

    for (const auto& requiredOpt : required) {
        if (!ap.has(requiredOpt)) {
            throw CommandError("missing required option: {}", requiredOpt);
        }
    }

    auto frame = currentFrame();
    auto name = *ap.get<std::string>(ARG_NAME);
    auto codeAddr = frame.evalToUInt64(*ap.get<std::string>(ARG_CODE_ADDR));
    auto filename = *ap.get<std::string>(ARG_FILENAME);
    auto dataAddrString = *ap.get<std::string>(ARG_DATA_ADDR);
    auto sizeString = *ap.get<std::string>(ARG_DATA_SIZE);
    auto dryRun = ap.has(ARG_DRY_RUN);

    probeList.push_back(ProbeSpec::create(
        codeAddr, dataAddrString, sizeString, name, filename, dryRun
    ));

    return doResync(ap);
}

bool ProbeCommand::doList(const CommandArgParser& ap) {
    println("There are {} probes.", probeList.size());
    for (const auto& probe : probeList) {
        println("- {}: code 0x{:08x} data '{}' size '{}' file '{}' (bp {})",
                probe->name, probe->codeAddr, probe->dataAddrString,
                probe->dataSizeString, probe->filenameTemplate,
                probe->bpId ? *probe->bpId : -1);
    }

    return true;
}

bool ProbeCommand::doRemove(const CommandArgParser& ap) {
    auto name = ap.get<std::string>(ARG_REMOVE);
    if (!name) { throw CommandError("missing probe name"); }
    auto probe = getProbeByName(*name);
    if (!probe) { throw CommandError("there is no probe by that name, try " \
        "--list"); }

    return removeProbeByName((*probe)->name);
}

bool ProbeCommand::doResync(const CommandArgParser& ap) {
    auto tgt = currentTarget();
    auto prc = currentProcess();
    for (auto& probe : probeList) {
        if (probe->set) continue;
        if (probe->bpId) {
            spdlog::trace("removing old bp {}...", *probe->bpId);
            std::ignore = tgt.removeBreakpointById(*probe->bpId);
        }

        spdlog::trace("creating new bp @ 0x{:08x}...", probe->codeAddr);
        auto bp = tgt.createBreakpointByAddress(probe->codeAddr);
        if (!bp) {
            throw CommandError("can't set a breakpoint on 0x{:08x}",
                               probe->codeAddr);
        } else {
            bp->SetAutoContinue(true);
            bp->SetCallback(
                breakpointCallback<ProbeSpec>, probe.get());
            probe->set = true;
            probe->bpId = bp->GetID();
        }
    }

    return true;
}

bool ProbeCommand::doClear(const CommandArgParser& ap) {
    while (probeList.size() > 0) {
        auto cur = probeList[0];
        if (!removeProbeByName(cur->name)) {
            spdlog::error("error deleting probe {}", cur->name);
        }
    }

    return probeList.empty();
}

std::optional<ProbeSpecRef> ProbeCommand::getProbeByName(const std::string& name) {
    for (const auto& probe : probeList) {
        if (probe->name == name) {
            return probe;
        }
    }

    return {};
}

bool ProbeCommand::removeProbeByName(const std::string& name) {
    auto oldSize = probeList.size();
    auto tgt = currentTarget();
    auto probe = getProbeByName(name);
    if (!probe) { throw CommandError("no probe by that name: {}", name); }

    if (tgt.hasBreakpointAtAddress((*probe)->codeAddr)) {
        tgt.removeBreakpointById(*(*probe)->bpId);
    } else {
        spdlog::warn("removing disabled probe: {}", name);
    }

    probeList.erase(std::remove_if(probeList.begin(), probeList.end(),
        [name] (const ProbeSpecRef& probe) {
            return probe->name == name;
        }), probeList.end());

    auto newSize = probeList.size();
    return oldSize != newSize;
}
