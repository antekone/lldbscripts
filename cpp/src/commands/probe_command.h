#pragma once

#include <memory>
#include "abstract_command.h"

struct ProbeSpec {
    uint64_t codeAddr;
    std::string dataAddrString;
    std::string dataSizeString;
    std::string name;
    std::string filenameTemplate;
    bool dryRun;

    bool set = false;
    int hitCount = 0;
    std::optional<lldb::break_id_t> bpId;

    ProbeSpec(uint64_t codeAddr,
        std::string dataAddrString,
        std::string dataSizeString,
        std::string name,
        std::string filenameTemplate,
        bool dryRun
    ) : codeAddr(codeAddr),
        dataAddrString(std::move(dataAddrString)),
        dataSizeString(std::move(dataSizeString)),
        name(std::move(name)),
        filenameTemplate(std::move(filenameTemplate)),
        dryRun(false) { }

    template <typename... Args>
    static std::shared_ptr<ProbeSpec> create(Args&&... args) {
        return std::make_shared<ProbeSpec>(std::forward<Args>(args)...);
    }

};
using ProbeSpecRef = std::shared_ptr<ProbeSpec>;

class ProbeCommand : public AbstractCommand {
public:
	void args(CommandArgParser& ap) override;
	bool run(const CommandArgParser& ap) override;
	std::string name() override { return "probe"; }
private:
	bool doInstall(const CommandArgParser& ap);
    bool doList(const CommandArgParser& ap);
    bool doRemove(const CommandArgParser& ap);
    bool doResync(const CommandArgParser& ap);
    bool doClear(const CommandArgParser& ap);

    std::optional<ProbeSpecRef> getProbeByName(const std::string& name);
    bool removeProbeByName(const std::string& name);
};