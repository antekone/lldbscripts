#include "target.h"
#include <SBBreakpointLocation.h>

std::optional<lldb::SBAddress> PTarget::toSBAddress(uint64_t address) {
    std::optional<lldb::SBModule> maxModule;
    std::optional<lldb::SBSection> maxSection;
    uint64_t maxLoadAddress = std::numeric_limits<uint64_t>::min();

    for (auto mod : modules()) {
        for (auto sect : mod.sections()) {
            auto loadAddress = sect.GetLoadAddress(tgt);
            if (loadAddress > address) continue;

            if (maxLoadAddress < loadAddress) {
                maxLoadAddress = loadAddress;
                maxModule = mod.raw();
                maxSection = sect;
            }
        }
    }

    if (!maxModule || !maxSection) return {};

    auto delta = address - maxSection->GetLoadAddress(tgt);
    return lldb::SBAddress { *maxSection, delta };
}

std::optional<lldb::SBBreakpoint> PTarget::createBreakpointByAddress(
    uint64_t address
) {
    auto bp = tgt.BreakpointCreateByAddress(address);
    if (bp.IsValid()) {
        return bp;
    } else {
        return {};
    }
}

bool PTarget::removeBreakpointByAddress(uint64_t address) {
    bool deletedEverything = true;
    bool deletedSomething = false;

    for (auto bp : breakpoints()) {
        for (size_t i = 0, len = bp.GetNumLocations(); i < len; i++) {
            auto loc = bp.GetLocationAtIndex(i);
            auto addr = loc.GetLoadAddress();
            if (addr == address) {
                if (!tgt.BreakpointDelete(bp.GetID())) {
                    deletedEverything = false;
                } else {
                    deletedSomething = true;
                }
            }
        }
    }

    return deletedSomething && deletedEverything;
}

bool PTarget::removeBreakpointById(lldb::break_id_t id) {
    auto bp = tgt.FindBreakpointByID(id);
    if (!bp.IsValid())
        return false;

    return tgt.BreakpointDelete(bp.GetID());
}

bool PTarget::hasBreakpointAtAddress(uint64_t address) {
    for (auto bp : breakpoints()) {
        for (size_t i = 0, len = bp.GetNumLocations(); i < len; i++) {
            auto loc = bp.GetLocationAtIndex(i);
            auto addr = loc.GetLoadAddress();
            if (addr == address) {
                return true;
            }
        }
    }

    return false;
}
