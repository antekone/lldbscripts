#pragma once

#include <cstdint>

template <typename Accessor>
concept AccessorCallback = requires(Accessor a) {
    a.init() == 0;
    a.get(0);
};

template <
    typename InnerType,
    AccessorCallback Accessor,
    typename Index = std::size_t
>
class GenericPropertyIterator {
public:
    explicit GenericPropertyIterator(
        Accessor acc,
        Index cur = 0
    ) : acc(acc), cur(cur) {
        max = acc.init();
    }

    GenericPropertyIterator& operator++() {
        cur++;
        return *this;
    }

    GenericPropertyIterator operator++(int) {
        auto temp = *this;
        ++(*this);
        return temp;
    }

    InnerType operator*() {
        return acc.get(cur);
    }

    bool operator!= (GenericPropertyIterator& other) const {
        return cur != other.cur;
    }

private:
    Index cur;
    Index max;
    Accessor acc;
};

template <typename Inner, typename Acc>
struct GenericPropertyRange {
    GenericPropertyIterator<Inner, Acc> start;
    GenericPropertyIterator<Inner, Acc> stop;

    GenericPropertyRange(
        GenericPropertyIterator<Inner, Acc> start,
        GenericPropertyIterator<Inner, Acc> stop
    ) : start(start), stop(stop) {}

    [[nodiscard]] auto begin() const { return start; }
    [[nodiscard]] auto end() const { return stop; }
};

