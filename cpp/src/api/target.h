#pragma once

#include <optional>
#include <limits>
#include <cstdint>

#include <SBTarget.h>
#include <SBModule.h>
#include <SBSection.h>
#include <SBAddress.h>
#include "module.h"
#include "iterators.h"

class PTarget;

struct ModuleAccessor {
    lldb::SBTarget self;
    explicit ModuleAccessor(const lldb::SBTarget& self) : self(self) {}

    [[nodiscard]] size_t init() const {
        return self.GetNumModules();
    }

    PModule get(size_t index) {
        return PModule(self.GetModuleAtIndex(index));
    }
};

struct BreakpointAccessor {
    lldb::SBTarget self;
    explicit BreakpointAccessor(const lldb::SBTarget& self) : self(self) {}

    [[nodiscard]] size_t init() const {
        return self.GetNumBreakpoints();
    }

    lldb::SBBreakpoint get(size_t index) {
        return self.GetBreakpointAtIndex(index);
    }
};

using ModuleIterator = GenericPropertyIterator<
    PModule, ModuleAccessor>;
using ModuleRange = GenericPropertyRange<
    PModule, ModuleAccessor>;

using BreakpointIterator = GenericPropertyIterator<
    lldb::SBBreakpoint, BreakpointAccessor>;
using BreakpointRange = GenericPropertyRange<
    lldb::SBBreakpoint, BreakpointAccessor>;

class PTarget {
public:
	explicit PTarget(const lldb::SBTarget& tgt) : tgt(tgt) { }

	lldb::SBTarget raw() { return tgt; }

    auto moduleIterator() {
        return ModuleIterator { ModuleAccessor(tgt) };
    }

    auto moduleEndIterator() {
        return ModuleIterator { ModuleAccessor(tgt), tgt.GetNumModules() };
    }

    auto breakpointIterator() {
        return BreakpointIterator { BreakpointAccessor(tgt) };
    }

    auto breakpointEndIterator() {
        return BreakpointIterator {
            BreakpointAccessor(tgt), tgt.GetNumBreakpoints()
        };
    }

    auto modules() {
        return ModuleRange { moduleIterator(), moduleEndIterator() };
    }

    auto breakpoints() {
        return BreakpointRange {
            breakpointIterator(), breakpointEndIterator()
        };
    }

    std::optional<lldb::SBAddress> toSBAddress(uint64_t address);
    std::optional<lldb::SBBreakpoint> createBreakpointByAddress(uint64_t address);
    bool hasBreakpointAtAddress(uint64_t address);
    bool removeBreakpointByAddress(uint64_t address);
    bool removeBreakpointById(lldb::break_id_t id);

private:
	lldb::SBTarget tgt;
};