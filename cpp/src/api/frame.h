#pragma once

#include <SBFrame.h>
#include <SBError.h>
#include <SBValue.h>
#include <SBThread.h>

#include <spdlog/spdlog.h>

#include "../errors/command_error.h"

class PFrame {
public:
	explicit PFrame(const lldb::SBFrame& f) : f(f) {

	}

	uint64_t evalToUInt64(const std::string& s) {
		lldb::SBError err;
		auto value = f.EvaluateExpression(s.c_str());
		if (err)
			throw CommandError(err, "unable to evaluate expression '{}'", s);

		auto ret = value.GetValueAsUnsigned(err);
		return ret;
	}

private:
	lldb::SBFrame f;
};