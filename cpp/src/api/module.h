#pragma once

#include <spdlog/spdlog.h>
#include <SBModule.h>
#include "iterators.h"

class PModule;

struct SectionAccessor {
    lldb::SBModule& self;

    explicit SectionAccessor(lldb::SBModule& self) : self(self) {}

    size_t init() { return self.GetNumSections(); }

    lldb::SBSection get(size_t index) {
        spdlog::trace("get section index={}", index);
        return self.GetSectionAtIndex(index);
    }
};

using SectionIterator = GenericPropertyIterator<lldb::SBSection, SectionAccessor>;
using SectionRange = GenericPropertyRange<lldb::SBSection, SectionAccessor>;

class PModule {
public:
    explicit PModule(const lldb::SBModule& mod) : mod(mod) {

    }

    lldb::SBModule& raw() {
        return mod;
    }

    auto sectionIterator() {
        return SectionIterator { SectionAccessor(mod), 0 };
    }

    auto sectionEndIterator() {
        return SectionIterator { SectionAccessor(mod), mod.GetNumSections() };
    }

    auto sections() {
        return SectionRange { sectionIterator(), sectionEndIterator() };
    }

private:
    lldb::SBModule mod;
};