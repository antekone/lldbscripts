#pragma once

#include <SBFileSpec.h>

class PFileSpec {
public:
	explicit PFileSpec(lldb::SBFileSpec spec) : spec(spec) {}

	std::string path() {
		std::string buf;
		const auto* dir = spec.GetDirectory();
		const auto* file = spec.GetFilename();

		if (dir) {
			buf += spec.GetDirectory();
			buf += "/";
		}

		if (file) {
			buf += spec.GetFilename();
		}

		return buf;
	}

private:
	lldb::SBFileSpec spec;
};