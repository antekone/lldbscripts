#pragma once

#include <vector>
#include <spdlog/spdlog.h>

#include <SBProcess.h>

class PProcess {
public:
	explicit PProcess(lldb::SBProcess proc) : proc(std::move(proc)) {

	}

	std::vector<uint8_t> queryMemory(uint64_t addr, size_t size) {
		auto buf = std::vector<uint8_t>();
		buf.resize(size);

		lldb::SBError err;
		size_t read = proc.ReadMemory(addr, buf.data(), size, err);
		if (read != size) {
			spdlog::trace("ReadMemory: I/O error");
			throw CommandError("memory I/O error: amount read (0x{:08X}, {}d)"
							   " is different than amount requested "
							   "(0x{:08X}, {}d)", read, read, size, size);
		}

		return buf;
	}

	lldb::SBProcess& raw() { return proc; }

private:
	lldb::SBProcess proc;
};