#pragma once

#include <exception>
#include <string>
#include <fmt/core.h>

#include <SBError.h>

class CommandError : public std::runtime_error {
public:
	template <typename... Args>
	explicit CommandError(const std::string& fmt, Args... args) :
		std::runtime_error(fmt::format(fmt::runtime(fmt), std::forward<Args>(args)...)) {

	}

	template <typename... Args>
	explicit CommandError(const lldb::SBError& err, const std::string& fmt, Args... args) :
		std::runtime_error(
			fmt::format("{}: {}", fmt::format(fmt::runtime(fmt), std::forward<Args>(args)...), err.GetCString() ? err.GetCString() : "(no reason given)"))
	{

	}
};

