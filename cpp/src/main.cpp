#include <SBCommandInterpreter.h>
#include <SBCommandReturnObject.h>
#include <SBDebugger.h>
#include <SBTypeCategory.h>
#include <SBTypeNameSpecifier.h>
#include <SBTypeSynthetic.h>

#include <spdlog/spdlog.h>
#include <spdlog/sinks/basic_file_sink.h>

#include "commands/dump_hex_command.h"
#include "commands/dump_to_file_command.h"
#include "commands/print_command.h"
#include "commands/bit_compare_command.h"
#include "commands/image_search_command.h"
#include "commands/symbol_search_command.h"
#include "commands/probe_command.h"

#include "pystr_bit_stream.h"

#define API __attribute__((used))

template <size_t N>
const char* py_code(const std::array<uint8_t, N>& code) {
	return reinterpret_cast<const char*>(code.data());
}

namespace lldb {
	void registerSyntheticTypes(lldb::SBDebugger& debugger) {
		auto cat1 = debugger.CreateCategory("TypeCat1");
		auto typeA1 = SBTypeSynthetic::CreateWithScriptCode(py_code(pystr_bit_stream_py));
		cat1.AddTypeSynthetic(SBTypeNameSpecifier("A1"), typeA1);
	}

	void registerCommands(lldb::SBDebugger& debugger) {
		lldb::SBCommandInterpreter interpreter = debugger.GetCommandInterpreter();
		lldb::SBCommand foo = interpreter.AddMultiwordCommand("a1", "lldbPowerPackA1 commands are grouped here");
		foo.AddCommand("dump-hex", new DumpHexCommand(), "hex-dumper for a selected memory range");
		foo.AddCommand("dump-to-file", new DumpToFileCommand(), "memory dumper, storing data to some file");
		foo.AddCommand("image-search", new ImageSearchCommand(), "image searcher");
		foo.AddCommand("symbol-search", new SymbolSearchCommand(), "symbol searcher");
		foo.AddCommand("print", new PrintCommand(), "print object command with custom renderers");
		foo.AddCommand("bit-cmp", new BitCompareCommand(), "compares bitfields");
		foo.AddCommand("probe", new ProbeCommand(), "install a probe");
	}

	void initLogging() {
//		auto fileLogger = spdlog::basic_logger_mt("lldbPowerPackA1", "logs/lldb.log");
//		spdlog::set_default_logger(fileLogger);
		spdlog::set_level(spdlog::level::trace);
	}

	void invokeStartupCommands(lldb::SBDebugger& debugger) {
		debugger.HandleCommand("type format add --format hex int");
		debugger.HandleCommand("type format add --format hex \"unsigned int\"");
		debugger.HandleCommand("type format add --format hex long");
		debugger.HandleCommand("type format add --format hex \"unsigned long\"");
	}

	API bool PluginInitialize(lldb::SBDebugger debugger) {
		spdlog::trace("--> PPA1 starting");
		initLogging();
		registerSyntheticTypes(debugger);
		registerCommands(debugger);
		invokeStartupCommands(debugger);
		return true;
	}
}
