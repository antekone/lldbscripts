import sys
import os


def compile_py(f_from, f_to):
    ddir = os.path.dirname(f_to)
    if not os.path.isdir(ddir):
        os.mkdir(ddir)

    with open(f_from, 'rb') as fp:
        with open(f_to, 'w') as fw:
            data_in = fp.read()
            fname = os.path.basename(f_from).replace(".", "_")

            outs = "#include <array>\n"
            outs += "#include <cstdint>\n"
            outs += "\n"
            outs += "static std::array<uint8_t, {}> pystr_{} {{\n    ".format(len(data_in), fname)
            col = 1

            for b in data_in:
                outs += "0x{:02X},".format(b)
                if col >= 16:
                    col = 0
                    outs += "\n    "
                col += 1

            if col > 0:
                outs += "\n"

            outs += "};\n"
            fw.write(outs)
    pass


compile_py(sys.argv[1], sys.argv[2])
exit(0)
