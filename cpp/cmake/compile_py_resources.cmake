include(FindPython3)

find_package(Python3 COMPONENTS Interpreter)

function (compile_py_resource path)
    get_filename_component(path_we ${path} NAME_WE)
    add_custom_command(
        COMMAND ${Python3_EXECUTABLE}
            ${CMAKE_CURRENT_SOURCE_DIR}/src/types/rescompiler.py
            ${CMAKE_CURRENT_SOURCE_DIR}/${path}
            ${CMAKE_CURRENT_BINARY_DIR}/pystrs/pystr_${path_we}.h
        DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/src/types/rescompiler.py ${path}
        OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/pystrs/pystr_${path_we}.h
    )
endfunction ()