include(CheckSourceCompiles)

function(lldb_check_api_type)
    set(CMAKE_REQUIRED_INCLUDES "${LLDB_API};${LLDB_API}/lldb/API")
    set(CMAKE_REQUIRED_LIBRARIES ${LLDB_SO_PATH})

    file(READ "${CMAKE_SOURCE_DIR}/cmake/lldb-api-autodetect-old.cpp" OLD_API_SRC)
    check_source_compiles(CXX "${OLD_API_SRC}" OLD_API_WORKS)
    file(READ "${CMAKE_SOURCE_DIR}/cmake/lldb-api-autodetect-new.cpp" NEW_API_SRC)
    check_source_compiles(CXX "${NEW_API_SRC}" NEW_API_WORKS)

    unset(CMAKE_REQUIRED_INCLUDES)
    unset(CMAKE_REQUIRED_LIBRARIES)

    if (OLD_API_WORKS)
        message(">> Old API works")
        set(HAVE_OLD_API TRUE PARENT_SCOPE)
    endif ()

    if (NEW_API_WORKS)
        message(">> New API works")
        set(HAVE_NEW_API TRUE PARENT_SCOPE)
    endif ()

    if (NOT OLD_API_WORKS)
        message(FATAL_ERROR ">> LLDB API doesn't allow to produce any output, failing")
    endif ()

    if (OLD_API_WORKS AND NOT NEW_API_WORKS)
        message(">> Old API works, and there is no new API, so enabling workarounds for GetSelectedThread()")
    endif ()

    if (NEW_API_WORKS)
        message(">> New API works, disabling workarounds for GetSelectedThread()")
    endif ()
endfunction()

function(autodetect_lldb_features)
    message(">> Discovering LLDB features...")

    lldb_check_api_type()

    set(HAVE_NEW_API ${HAVE_NEW_API} PARENT_SCOPE)
    set(HAVE_OLD_API ${HAVE_OLD_API} PARENT_SCOPE)
endfunction ()
