use lldb::SBDebugger;

#[no_mangle]
pub extern "C" fn _ZN4lldb16PluginInitializeENS_10SBDebuggerE(dbg: lldb_sys::SBDebuggerRef) -> bool {
    println!("rust loaded");
    let dbg = lldb::SBDebugger::from(unsafe { lldb_sys::CloneSBDebugger(dbg) });
    let interp = dbg.command_interpreter();
    true
}