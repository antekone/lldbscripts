import sys
import lldb
sys.path.append("/home/antek/dev/haxe/lldbscripts/bin")
import main
import argparse

def cmd_db(ctx, cmd, exe_ctx, result, state):
    thread =  exe_ctx.thread
    print("Return of GetSelectedThread() is: {}".format(thread.GetThreadID()))
