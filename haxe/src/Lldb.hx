package;

class SBDebugger {
    var dbg: Dynamic;

    public function new(dbg: Dynamic) {
        this.dbg = dbg;
    }

    public function getVersionString(): String {
        return this.dbg.GetVersionString();
    }
}

class SBCommandReturnObject {
    var ret: Dynamic;

    public function new(ret: Dynamic) {
        this.ret = ret;
    }
}
