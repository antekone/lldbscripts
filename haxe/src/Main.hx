import python.Dict;
import Lldb;

class CommandDB {
    public function new() {
    }

    public function run() {
    }
}

@:keep
class Main {
    var commands = [new CommandDB()];

    static public function main(): Void {
    }

    static public function commandDB(dbg: SBDebugger, command: String, result: SBCommandReturnObject, state: python.Dict<String, Dynamic>) {
        trace('dbg version: ${dbg.getVersionString()}');
        trace('commandline: "$command"');
        state.set("Aaa", "bb");
        trace('x: ${state.get("Aaa")}');
    }
}

